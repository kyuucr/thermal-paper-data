const cliProgress = require("cli-progress");
const dataUtils = require('./data-utils');
const logger = require("./logger");
const fs = require('fs');
const path = require('path');
const powerUtils = require('./power-utils');

let sortAsc = (a,b) => {return a - b};
let sortDsc = (a,b) => {return b - a};
let avgReduceDbm = (avg, value, _, { length }) => avg + powerUtils.dbmToW(value) / length;

let container = {}
let limit = {
    lte: 16,    // Per band
    nr: 16,     // Per FR1, FR2
    wifi2: 57,
    wifi5: 127
}
let nullNumber = NaN;
let useBandDesignation = false;
let singleOutputWriteStream;

const createHeader = function() {
    let header = `location_type,gps_accuracy,datetimeIso,date,time,zone,n1_wifi_2_4,2_4_avg_rssi,`;
    for (let j = 1; j <= limit.wifi2; j++){
        header += `2_4_rssi_${j},`;
    }
    header += `n2_wifi_5,5_avg_rssi,`;
    for (let j = 1; j <= limit.wifi5; j++){
        header += `5_rssi_${j},`;
    }
    let temp = 3;
    let bandList = dataUtils.getBands();
    for (let band of bandList) {
        header += `n${temp++}_lte_${useBandDesignation ? dataUtils.getBandDesignation(band) + "_" : ""}band_${band},band_${band}_avg_rsrp,`;
        for (let j = 1, length2 = limit.lte; j <= length2; j++){
            header += `band_${band}_rsrp_${j},`;
        }
        header += `band_${band}_avg_rsrq,`;
        for (let j = 1, length2 = limit.lte; j <= length2; j++){
            header += `band_${band}_rsrq_${j},`;
        }
        header += `band_${band}_avg_rssi,`;
        for (let j = 1, length2 = limit.lte; j <= length2; j++){
            header += `band_${band}_rssi_${j},`;
        }
    }
    header += `n${temp}_nr_fr1,nr_fr1_avg_rsrp,`;
    for (let j = 1; j <= limit.nr; j++){
        header += `nr_fr1_rsrp_${j},`;
    }
    header += `nr_fr1_avg_rsrq,`;
    for (let j = 1; j <= limit.nr; j++){
        header += `nr_fr1_rsrq_${j},`;
    }
    header += `n${temp}_nr_fr2,nr_fr2_avg_rsrp,`;
    for (let j = 1; j <= limit.nr; j++){
        header += `nr_fr2_rsrp_${j},`;
    }
    header += `nr_fr2_avg_rsrq,`;
    for (let j = 1; j <= limit.nr; j++){
        header += `nr_fr2_rsrq_${j},`;
    }
    header += `\n`;
    return header;
}

let mlCsv = {
    setLimit: function(newLimit) {
        for (let limitName in newLimit) {
            limit[limitName] = newLimit[limitName];
        }
    },

    insert: function(outputPath, contentJson, zipFilePath) {
        if (container[zipFilePath] === undefined) {
            container[zipFilePath] = {
                outputPath: outputPath,
                strings: []
            }
        }

        let currLocationLabel = "undefined";
        if (zipFilePath.includes("mostly indoor")) {
            currLocationLabel = "mostly indoor";
        } else if (zipFilePath.includes("mostly outdoor")) {
            currLocationLabel = "mostly outdoor";
        } else if (zipFilePath.includes("indoor")) {
            currLocationLabel = "indoor";
        } else if (zipFilePath.includes("outdoor")) {
            currLocationLabel = "outdoor";
        }

        // Write prefixes
        let prefixString = `${currLocationLabel},${contentJson.location.hor_acc},${contentJson.datetimeIso},${contentJson.datetime.date},${contentJson.datetime.time},${contentJson.datetime.zone},`;

        // Get 2.4 and 5 Wifi
        let holder2 = [], holder5 = [];
        for (let k = 0, length3 = contentJson.wifi_info.length; k < length3; k++){
            if (contentJson.wifi_info[k].rssi === 2147483647) continue; // wrong value
            if (contentJson.wifi_info[k].primaryFreq < 5000) {
                holder2.push(contentJson.wifi_info[k].rssi);
            } else {
                holder5.push(contentJson.wifi_info[k].rssi);
            }
        }

        // Write Wifi 2.4
        holder2.sort(sortDsc);
        let tempAvg = (holder2.length === 0) ? nullNumber : powerUtils.wToDbm(holder2.reduce(avgReduceDbm, null, 0));
        let wifi2_4String = `${holder2.length},${tempAvg},`;
        for (let i = 0, length = holder2.length; i < limit.wifi2; i++) {
            if (i < length) {
                wifi2_4String += `${holder2[i]},`;
            } else {
                wifi2_4String += `${nullNumber},`;
            }
        }
        // Write Wifi 5
        holder5.sort(sortDsc);
        tempAvg = (holder5.length === 0) ? nullNumber : powerUtils.wToDbm(holder5.reduce(avgReduceDbm, null, 0));
        let wifi5String = `${holder5.length},${tempAvg},`;
        for (let i = 0, length = holder5.length; i < limit.wifi5; i++) {
            if (i < length) {
                wifi5String += `${holder5[i]},`;
            } else {
                wifi5String += `${nullNumber},`;
            }
        }

        // Get LTE
        let holderLteRsrp = {}, holderLteRsrq = {}, holderLteRssi = {}, lteStrings = {};
        for (let k = 0, length3 = contentJson.cell_info.length; k < length3; k++){
            if (contentJson.cell_info[k].rsrp === 2147483647
                || contentJson.cell_info[k].rssi === 2147483647
                || contentJson.cell_info[k].rsrq === 2147483647
                || contentJson.cell_info[k].band === 0) continue; // Wrong value
            let band = contentJson.cell_info[k].band;
            if (holderLteRsrp[band] === undefined) holderLteRsrp[band] = [];
            if (holderLteRsrq[band] === undefined) holderLteRsrq[band] = [];
            if (holderLteRssi[band] === undefined) holderLteRssi[band] = [];
            if (lteStrings[band] === undefined) lteStrings[band] = [];
            holderLteRsrp[band].push(contentJson.cell_info[k].rsrp);
            holderLteRsrq[band].push(contentJson.cell_info[k].rsrq);
            holderLteRssi[band].push(contentJson.cell_info[k].rssi);
        }
        // Write LTE
        for (let band in holderLteRsrp) {
            let tempString = "";
            // RSRP
            holderLteRsrp[band].sort(sortDsc);
            tempAvg = (holderLteRsrp[band].length === 0) ? nullNumber : powerUtils.wToDbm(holderLteRsrp[band].reduce(avgReduceDbm, null, 0));
            tempString += `${holderLteRsrp[band].length},${tempAvg},`;
            for (let i = 0, length = holderLteRsrp[band].length; i < limit.lte; i++) {
                if (i < length) {
                    tempString += `${holderLteRsrp[band][i]},`;
                } else {
                    tempString += `${nullNumber},`;
                }
            }
            // RSRQ
            holderLteRsrq[band].sort(sortDsc);
            tempAvg = (holderLteRsrq[band].length === 0) ? nullNumber : powerUtils.wToDbm(holderLteRsrq[band].reduce(avgReduceDbm, null, 0));
            tempString += `${tempAvg},`;
            for (let i = 0, length = holderLteRsrq[band].length; i < limit.lte; i++) {
                if (i < length) {
                    tempString += `${holderLteRsrq[band][i]},`;
                } else {
                    tempString += `${nullNumber},`;
                }
            }
            // RSSI
            holderLteRssi[band].sort(sortDsc);
            tempAvg = (holderLteRssi[band].length === 0) ? nullNumber : powerUtils.wToDbm(holderLteRssi[band].reduce(avgReduceDbm, null, 0));
            tempString += `${tempAvg},`;
            for (let i = 0, length = holderLteRssi[band].length; i < limit.lte; i++) {
                if (i < length) {
                    tempString += `${holderLteRssi[band][i]},`;
                } else {
                    tempString += `${nullNumber},`;
                }
            }
            lteStrings[band] = tempString;
        }

        let nrString = "";
        let holderNrFR1Rsrp = [], holderNrFR1Rsrq = [], holderNrFR2Rsrp = [], holderNrFR2Rsrq = [];
        if (contentJson.nr_info) {
            // Get NR RSRP - FR1 or FR2
            for (let k = 0, length3 = contentJson.nr_info.length; k < length3; k++){
                if (contentJson.nr_info[k].ssRsrp === 2147483647 || contentJson.nr_info[k].ssRsrq === 2147483647) continue; // wrong value
                if (dataUtils.getServiceState(contentJson, "nrFrequencyRange") === "mmWave") {
                    holderNrFR2Rsrp.push(contentJson.nr_info[k].ssRsrp);
                    holderNrFR2Rsrq.push(contentJson.nr_info[k].ssRsrq);
                } else {
                    holderNrFR1Rsrp.push(contentJson.nr_info[k].ssRsrp);
                    holderNrFR1Rsrq.push(contentJson.nr_info[k].ssRsrq);
                }
            }
        }
        // Write NR FR1
        holderNrFR1Rsrp.sort(sortDsc);
        tempAvg = (holderNrFR1Rsrp.length === 0) ? nullNumber : powerUtils.wToDbm(holderNrFR1Rsrp.reduce(avgReduceDbm, null, 0));
        nrString += `${holderNrFR1Rsrp.length},${tempAvg},`;
        for (let i = 0, length = holderNrFR1Rsrp.length; i < limit.nr; i++) {
            if (i < length) {
                nrString += `${holderNrFR1Rsrp[i]},`;
            } else {
                nrString += `${nullNumber},`;
            }
        }
        holderNrFR1Rsrq.sort(sortDsc);
        tempAvg = (holderNrFR1Rsrq.length === 0) ? nullNumber : powerUtils.wToDbm(holderNrFR1Rsrq.reduce(avgReduceDbm, null, 0));
        nrString += `${tempAvg},`;
        for (let i = 0, length = holderNrFR1Rsrq.length; i < limit.nr; i++) {
            if (i < length) {
                nrString += `${holderNrFR1Rsrq[i]},`;
            } else {
                nrString += `${nullNumber},`;
            }
        }
        // Write NR FR2
        holderNrFR2Rsrp.sort(sortDsc);
        tempAvg = (holderNrFR2Rsrp.length === 0) ? nullNumber : powerUtils.wToDbm(holderNrFR2Rsrp.reduce(avgReduceDbm, null, 0));
        nrString += `${holderNrFR2Rsrp.length},${tempAvg},`;
        for (let i = 0, length = holderNrFR2Rsrp.length; i < limit.nr; i++) {
            if (i < length) {
                nrString += `${holderNrFR2Rsrp[i]},`;
            } else {
                nrString += `${nullNumber},`;
            }
        }
        holderNrFR2Rsrq.sort(sortDsc);
        tempAvg = (holderNrFR2Rsrq.length === 0) ? nullNumber : powerUtils.wToDbm(holderNrFR2Rsrq.reduce(avgReduceDbm, null, 0));
        nrString += `${tempAvg},`;
        for (let i = 0, length = holderNrFR2Rsrq.length; i < limit.nr; i++) {
            if (i < length) {
                nrString += `${holderNrFR2Rsrq[i]},`;
            } else {
                nrString += `${nullNumber},`;
            }
        }

        // END
        container[zipFilePath].strings.push({
            prefix: prefixString,
            wifi2_4: wifi2_4String,
            wifi5: wifi5String,
            lteStrings: lteStrings,
            nr: nrString
        });
    },

    write: function(zipFilePath) {
        if (container[zipFilePath] === undefined) {
            logger.w(`No ML data for ${zipFilePath} (probably due to filtering)`);
            return;
        }

        // Create new file or use single output
        let bandList = dataUtils.getBands();
        let os = singleOutputWriteStream;
        let strings = container[zipFilePath].strings;
        if (singleOutputWriteStream === undefined) {
            let filename = zipFilePath.replace(/\//g, "_") + ".csv";
            let outputPath = path.join(container[zipFilePath].outputPath, filename);

            os = fs.createWriteStream(outputPath);
            logger.d(`Writing to: ${outputPath}, location label: ${strings[0].prefix.split(",")[0]}, # of data: ${strings.length}`);
            // Header
            os.write(createHeader());
        } else {
            logger.d(`Writing from: ${zipFilePath}, location label: ${strings[0].prefix.split(",")[0]}, # of data: ${strings.length}`);
        }

        // Write csv
        for (let j = 0, length2 = strings.length; j < length2; j++) {
            let tempString = strings[j].prefix;

            // Wifi 2.4
            tempString += strings[j].wifi2_4;
            // Wifi 5
            tempString += strings[j].wifi5;

            // LTE
            for (let band of bandList) {
                if (strings[j].lteStrings[band] === undefined) {
                    // RSRP
                    tempString += `0,${nullNumber},`
                    for(let k = 0; k < limit.lte; k++){
                        tempString += `${nullNumber},`;
                    }
                    // RSRQ
                    tempString += `${nullNumber},`
                    for(let k = 0; k < limit.lte; k++){
                        tempString += `${nullNumber},`;
                    }
                    // RSSI
                    tempString += `${nullNumber},`
                    for(let k = 0; k < limit.lte; k++){
                        tempString += `${nullNumber},`;
                    }
                } else {
                    // RSRP, RSRQ, RSSI
                    tempString += strings[j].lteStrings[band];
                }
            }

            // NR
            tempString += strings[j].nr;

            tempString += `\n`;
            os.write(tempString);
        }
        if (singleOutputWriteStream === undefined) {
            os.close();
        }
        delete container[zipFilePath];
    },

    writeAll: function(singleOutputPath = "") {
        if (singleOutputPath !== "") {
            singleOutputWriteStream = fs.createWriteStream(singleOutputPath);
            // Header
            singleOutputWriteStream.write(createHeader());
        }
        for (let zipFilePath in container) {
            this.write(zipFilePath);
        }
        if (singleOutputPath !== "") {
            singleOutputWriteStream.close();
        }
    }
}

module.exports = mlCsv;