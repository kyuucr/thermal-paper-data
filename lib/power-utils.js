let powerUtils = {
    dbmToW: function(powDbm) {
        return Math.pow(10, (powDbm / 10)) / 1000;
    },

    wToDbm: function(powW) {
        return 10 * Math.log10(1000 * powW);
    }
}

module.exports = powerUtils;