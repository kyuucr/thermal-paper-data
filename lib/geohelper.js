let geohelper = {

    CHANNELS: [36, 38, 40, 42, 44, 46, 48, 149, 151, 153, 155, 157, 159, 161, 165],
    MIN_RSRP_WEIGHT: -140,
    RANGE_RSRP_WEIGHT: 65,
    MIN_RSSI_WEIGHT: -95,
    RANGE_RSSI_WEIGHT: 50,

    measure: function(lat1, lon1, lat2, lon2) {  // generally used geo measurement function
        let R = 6378.137; // Radius of earth in KM
        let dLat = lat2 * Math.PI / 180 - lat1 * Math.PI / 180;
        let dLon = lon2 * Math.PI / 180 - lon1 * Math.PI / 180;
        let a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
        Math.sin(dLon/2) * Math.sin(dLon/2);
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        let d = R * c;
        return d * 1000; // meters
    },

    addLatitude: function(lat, lon, dist) {
        dist = dist / 1000; // to km
        let R = 6378.137; // Radius of earth in KM
        return (lat + (dist / R) * (180 / Math.PI));
    },

    addLongitude: function(lat, lon, dist) {
        dist = dist / 1000; // to km
        let R = 6378.137; // Radius of earth in KM
        return (lon + (dist / R) * (180 / Math.PI) / Math.cos(lat * Math.PI/180));
    },

    grahamScan: function(locations) {
        let orientation = function(p0, a, b) {
            return (a.latitude - p0.latitude) * (b.longitude - p0.longitude) -
                   (a.longitude - p0.longitude) * (b.latitude - p0.latitude);
        }

        // Find bottommost point
        let min = locations[0], minIdx = 0;
        for(let i = 1, length1 = locations.length; i < length1; i++){
            if (locations[i].latitude < min.latitude
                || (locations[i].latitude === min.latitude
                    && locations[i].longitude < min.longitude)) {
                min = locations[i];
                minIdx = i;
            }
        }

        // Remove min from locations
        locations.splice(minIdx, 1);

        // Sort locations based on the orientation to min
        locations.sort((a, b) => {
            let orValue = orientation(min, a, b);

            if (orValue === 0) {
                let distA = Math.pow((min.longitude - a.longitude), 2) + Math.pow((min.latitude - a.latitude), 2);
                let distB = Math.pow((min.longitude - b.longitude), 2) + Math.pow((min.latitude - b.latitude), 2);
                return (distB >= distA) ? -1 : 1;
            } else if (orValue > 0) {
                return 1;
            } else {
                return -1;
            }
        });

        // Remove points with the same angle
        let locCopy = [];
        for(let i = 0, length1 = locations.length; i < length1; i++){
            while (i < length1 && orientation(min, locations[i], locations[i+1] === 0)) {
                i++;
            }
            locCopy.push(locations[i]);
        }
        locations = locCopy;
        // If filtered array length < 3, convex hull not possible
        if (locations.length < 3) return [];

        // Create stack
        let stack = [];
        stack.push(min);
        stack.push(locations[0]);
        stack.push(locations[1]);

        for(let i = 2, length1 = locations.length; i < length1; i++){
            while (stack.length > 1 && orientation(stack[stack.length-2], stack[stack.length-1], locations[i]) >= 0) {
                stack.pop();
            }
            stack.push(locations[i]);
        }

        return stack;
    }
};

module.exports = geohelper;