const agg = require('./lib/aggregator');
const csvUtils = require('./lib/csv-utils');
const dataUtils = require('./lib/data-utils');
const filter = require('./lib/filter');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);
if (ARGS.length < 2) {
    console.log("Not enough argument!");
    console.log(`Usage: nodejs ${path.basename(__filename)} <input folder> <output folder> [--label-location] [--multiple-outputs] [BSSID filters ...]`);
    process.exit(1);
}

let inputFolder = ARGS[0];
let outputPath = ARGS[1];
let bssidFilter = [];
let jsonFilter = [];
let isLocationLabel = false;
let useOneOutput = true;
const MAX_LINE = 1e6;

if (ARGS.length > 2) {
    for(let i = 2, length1 = ARGS.length; i < length1; i++){
        if (ARGS[i] === "--label-location") {
            isLocationLabel = true;
        } else if (ARGS[i] === "--multiple-outputs") {
            useOneOutput = false;
        } else {
            // Try to parse to JSON
            try {
                let json = JSON.parse(ARGS[i]);
                jsonFilter.push(json);
            } catch (e) {
                bssidFilter.push(ARGS[i]);
            }
        }
    }
}

let os;
if (useOneOutput) {
    os = fs.createWriteStream(outputPath);
}
let header = `timestamp,latitude,longitude,altitude,hor_acc,ver_acc,${isLocationLabel ? ("location_category,") : ""}`
            + `uuid,device_model,ssid,bssid,primaryFreq,centerFreq0,centerFreq1,width,channelNum,primaryChNum,rssi,standard,`
            + `connected,linkSpeed,txLinkSpeed,rxLinkSpeed,maxSupportedTxLinkSpeed,maxSupportedRxLinkSpeed,capabilities,`
            + `timestampDeltaMs,extra\n`;

let filenameIdx = 0;
let currLine = 0;
agg.callbackJsonRecursive(inputFolder, data => {
    if (data.type !== "sigcap") return;
    let curPath = data.path;

    if (!useOneOutput) {
        fs.mkdirSync(outputPath, { recursive: true }, (err) => {
            console.log("ERROR: cannot make output dir");
        });
        filePath = path.join(outputPath, `${curPath.replace(/\/|\\/g,"-")}.csv`);
        os.createWriteStream(filePath);
    } else {
        filePath = outputPath;
    }
    let currLocationLabel = ""
    if (isLocationLabel) {
        if (curPath.includes("indoor")) {
            currLocationLabel = "indoor";
        } else if (curPath.includes("outdoor")) {
            currLocationLabel = "outdoor"
        } else {
            currLocationLabel = "undefined"
        }
    }

    let currJson = data.json;
    if (jsonFilter.length > 0) {
        currJson = filter.filterArray(jsonFilter, currJson);
    }

    if (!useOneOutput || currLine === 0) {
        os.write(header);
    }

    if (isLocationLabel) console.log("Location label:", currLocationLabel)
    console.log(`processing... path: ${data.path}, # of files: ${data.filePath.length}, # of data: ${currJson.length}`);

    for(let j = 0, length2 = currJson.length; j < length2; j++){
        let string = "";
        for(let k = 0, length3 = currJson[j].wifi_info.length; k < length3; k++){
            let wifiEntry = currJson[j].wifi_info[k];
            let idx = wifiEntry.bssid;
            if (bssidFilter.length === 0 || bssidFilter.includes(idx)) {
                string += `${currJson[j].datetimeIso},${currJson[j].location.latitude},${currJson[j].location.longitude},${currJson[j].location.altitude},`
                        + `${currJson[j].location.hor_acc},${currJson[j].location.ver_acc},${isLocationLabel ? (currLocationLabel + ",") : ""}`
                        + `${dataUtils.cleanString(currJson[j].uuid)},${dataUtils.cleanString(currJson[j].deviceName)},`
                        + `${dataUtils.cleanString(wifiEntry.ssid)},${dataUtils.cleanString(wifiEntry.bssid)},${dataUtils.cleanNumeric(wifiEntry.primaryFreq)},`
                        + `${dataUtils.cleanNumeric(wifiEntry.centerFreq0)},${dataUtils.cleanNumeric(wifiEntry.centerFreq1)},${dataUtils.cleanNumeric(wifiEntry.width)},`
                        + `${dataUtils.cleanNumeric(wifiEntry.channelNum)},${dataUtils.cleanNumeric(wifiEntry.primaryChNum)},${dataUtils.cleanSignal(wifiEntry.rssi)},`
                        + `${dataUtils.cleanString(wifiEntry.standard)},${dataUtils.cleanString(wifiEntry.connected)},${dataUtils.cleanNumeric(wifiEntry.linkSpeed)},`
                        + `${dataUtils.cleanNumeric(wifiEntry.txLinkSpeed)},${dataUtils.cleanNumeric(wifiEntry.rxLinkSpeed)},${dataUtils.cleanNumeric(wifiEntry.maxSupportedTxLinkSpeed)},`
                        + `${dataUtils.cleanNumeric(wifiEntry.maxSupportedRxLinkSpeed)},${dataUtils.cleanString(wifiEntry.capabilities)},${dataUtils.cleanNumeric(wifiEntry.timestampDeltaMs)},`
                let extras = dataUtils.cleanString(wifiEntry.extra);
                if (extras === "") {
                    extras = "N/A";
                } else {
                    extras = `"${extras.replace(/\"/g, "'")}"`
                }
                string += `${extras}\n`;
                currLine++;
            }
        }
        os.write(string);
        if (currLine > MAX_LINE) {
            os.close();
            currLine = 0;
            filePath = filePath.replace(/-?\d*\.csv$/, `-${++filenameIdx}.csv`);
            os = fs.createWriteStream(filePath);
            os.write(header);
            console.log(`Output full, new file: ${filePath}`);
        }
    }

    if (!useOneOutput) {
        os.close();
        filenameIdx = 0;
    } else {
        outputPath = filePath;
    }
});

if (useOneOutput) {
    os.close();
}