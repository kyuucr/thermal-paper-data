const agg = require('./lib/aggregator');
const commandParser = require('./lib/command-parser');
const csvUtils = require('./lib/csv-utils');
const dataUtils = require('./lib/data-utils');
const statsUtils = require('./lib/stats-utils');
const fs = require('fs');
const path = require('path');


// Fn & var declarations
const fileWritten = {};
const writeToFile = function(path, data, header, isAppend = false) {
    if (isAppend && fileWritten[path] === true) {
        fs.appendFileSync(path, data);
    } else {
        if (isAppend) {
            fileWritten[path] = true;
        }
        let fullData = (header ? header : "") + data;
        fs.writeFileSync(path, fullData);
    }
}

const headerTput = `"Elapsed time (s)" "FCC Throughput (Mbps)"\n`;
const bucket = {};
const testIdBin = [];


// Process arguments
commandParser.addArgument("input dir", { defaultValue: "./input" });
commandParser.addArgument("output dir", { defaultValue: "./plots/002-time-vs-nsgdl-temps/data" });
commandParser.addArgument("time bucket", { keyword: "--time-bucket", defaultValue: 5000 });
const options = commandParser.parse();

const metadataList = agg.getFileListRecursive(options["input dir"], /metadata-.*.json$/);
metadataList.forEach(metadataFile => {

    console.log(`Processing... ${metadataFile}`);
    const metadata = JSON.parse(fs.readFileSync(metadataFile));
    const inputDir = metadataFile.replace(new RegExp(`${path.sep}metadata-|\\.json$`, "g"), "/");

    // Process timespan
    let nsgData = {};
    let timespans = metadata.timespans;
    for (let i = 0, length1 = timespans.length; i < length1; i++) {
        timespans[i].start = new Date(timespans[i].start).getTime();
        timespans[i].end = new Date(timespans[i].end).getTime();
        bucket[i] = []
    }

    // Skip FCC ST only data
    if (metadata.description.match(/FCC ST Only/i) !== null) {
        return;
    }

    const isWinter = path.basename(inputDir).startsWith("01");
    const isChicago = metadataFile.match(/09-2(8|9)/) === null;
    const isMiami = metadataFile.match(/09-29/) === null;
    let pathSplit = inputDir.split(path.sep);
    const outputDir = path.join(options["output dir"], pathSplit[1], pathSplit[2]);
    const prefix = pathSplit[1];
    console.log(`is winter: ${isWinter}; is Chicago: ${isChicago}`);
    console.log(`Desc.: ${metadata.description}`);
    console.log(`Input dir: ${inputDir}`);
    console.log(`Output dir: ${outputDir}`);


    const fccBin = [];;
    agg.callbackJsonRecursive(inputDir, data => {
        let json = data.json;
        let dataType = dataUtils.getJSONType(json[0]);
        if (dataType == "fcc") {
            console.log(`Folder: ${data.path}; Data type: ${dataType}; # of data: ${json.length}`);
            // Take FCC DL test time for filtering sigcap and nsg data
            for(let i = 0, length1 = json.length; i < length1; i++) {
                let testId = json[i].tests.testId;
                // Skip duplicates testId
                if (testId === undefined || testIdBin.includes(testId)) continue;
                testIdBin.push(testId);

                // Check if download test exist and not 0
                let downloadTest = json[i].tests.download;
                if (downloadTest === undefined
                        || downloadTest.local_datetime === undefined
                        || downloadTest.throughput === 0) {
                    continue;
                }

                // Get valid time idx
                let time = new Date(downloadTest.local_datetime).getTime();
                let idx;
                for (let j = 0, length2 = timespans.length; j < length2; j++) {
                    if (time > timespans[j].start && time < timespans[j].end) {
                        idx = j;
                        break;
                    }
                }
                if (idx === undefined) {
                    continue;
                }
                bucket[idx].push({
                    time: parseFloat(time - timespans[idx].start) / 1000,
                    dlMbps: downloadTest.throughput * 8 / 1e6,
                    type: dataUtils.getNetworkType(json[i])
                });
            }
        }
    }); // End of agg.callbackJsonRecursive

    // Create output folder if not exist
    fs.mkdirSync(outputDir, { recursive: true });

    for (let idx in bucket) {
        if (bucket[idx].length === 0) {
            continue;
        }
        let stringOut = "", stringOutNr = "", stringOutLte = "";
        let currBucket = bucket[idx].sort((a, b) => a.time - b.time);
        let startTime = currBucket[0].time;
        for (let data of currBucket) {
            stringOut += `${data.time - startTime} ${data.dlMbps}\n`;
            if (data.type.startsWith("NR")) {
                stringOutNr += `${data.time - startTime} ${data.dlMbps}\n`;
            } else if (data.type === "LTE") {
                stringOutLte += `${data.time - startTime} ${data.dlMbps}\n`;
            }
        }
        // writeToFile(path.join(outputDir, `${idx}-fccDlMbps-all.dat`),
        //         stringOut, headerTput);
        if (isChicago) {
            writeToFile(path.join(options["output dir"], prefix, `chi-fccDlMbps-all.dat`),
                    stringOut, headerTput, true);
            writeToFile(path.join(options["output dir"], prefix, `chi-fccDlMbps-nr.dat`),
                    stringOutNr, headerTput, true);
            writeToFile(path.join(options["output dir"], prefix, `chi-fccDlMbps-lte.dat`),
                    stringOutLte, headerTput, true);
            if (isWinter) {
                writeToFile(path.join(options["output dir"], prefix, `winter-chi-fccDlMbps-all.dat`),
                        stringOut, headerTput, true);
                writeToFile(path.join(options["output dir"], prefix, `winter-chi-fccDlMbps-nr.dat`),
                        stringOutNr, headerTput, true);
                writeToFile(path.join(options["output dir"], prefix, `winter-chi-fccDlMbps-lte.dat`),
                        stringOutLte, headerTput, true);
            } else {
                writeToFile(path.join(options["output dir"], prefix, `summer-chi-fccDlMbps-all.dat`),
                        stringOut, headerTput, true);
                writeToFile(path.join(options["output dir"], prefix, `summer-chi-fccDlMbps-nr.dat`),
                        stringOutNr, headerTput, true);
                writeToFile(path.join(options["output dir"], prefix, `summer-chi-fccDlMbps-lte.dat`),
                        stringOutLte, headerTput, true);
            }
        }
    }
});

console.log(`Done!`);
