const agg = require('./lib/aggregator');
const csvUtils = require('./lib/csv-utils');
const dataUtils = require('./lib/data-utils');
const filter = require('./lib/filter');
const fs = require('fs');
const path = require('path');

let args = process.argv.slice(2);
if (args.length < 1) {
    console.log("Not enough argument!");
    console.log(`Usage: nodejs ${path.basename(__filename)} <input folder> [output path] [--filter <filter JSON string>]`);
    process.exit(1);
}

let outputPath = "output-sigcap.csv";
let inputFolder = args[0];
let inputFilter;
args.splice(0, 1);
while (args.length > 0) {
    switch (args[0]) {
        case "--filter": {
            try {
                inputFilter = JSON.parse(args[1]);
            } catch (err) {
                console.log(`Filter parsing error!`, err);
                process.exit(1);
            }
            args.splice(0, 2);
            break;
        }
        default: {
            outputPath = args[0];
            args.splice(0, 1);
            break;
        }
    }
}
console.log(`Input folder: ${inputFolder}`);

console.log(`Writing to: ${outputPath}`);
let os = fs.createWriteStream(outputPath);

// Write header
let header = `operator,sim_operator,carrier,device_id,device_model,timestamp,latitude,longitude,altitude,hor_acc,ver_acc,nrStatus,nrAvailable,dcNrRestricted,enDcAvailable,nrFrequencyRange,ci,usingCA,cellBandwidths,sumBw,lte/nr,`;
header += `pci,band,earfcn,bandwidth,rsrp,rsrq,rssi,primary/other,\n`;
os.write(header);

hashBin = [];

// Get SigCap using callback since we can have data chunks of 140 MB file
agg.callbackJsonRecursive(inputFolder, data => {
    let sigcap = data.json;
    if (inputFilter) {
        sigcap = filter.filterArray(inputFilter, sigcap);
    }
    console.log(`processing... path: ${data.path}, # of files: ${data.filePath.length}, # of data: ${sigcap.length}`);

    // Write csv
    for(let i = 0, length1 = sigcap.length; i < length1; i++){
        // Sanity checks
        let hash = dataUtils.hashObj(sigcap[i]);
        if (hashBin.includes(hash)) continue;
        hashBin.push(hash);
        if (sigcap[i].opName === undefined && sigcap[i].simName === undefined && sigcap[i].carrierName === undefined) continue;
        let op = dataUtils.getCleanOp(sigcap[i]);
        if (op === "Unknown") continue;

        // Handle old datetime
        let datetimeIso = dataUtils.getCleanDatetime(sigcap[i]);

        // Get LTE Primary
        for(let j = 0, length2 = sigcap[i].cell_info.length; j < length2; j++){
            if (dataUtils.isLtePrimary(sigcap[i].cell_info[j])) {
                // Write overview
                let entry = `${op},${sigcap[i].simName ? sigcap[i].simName : "N/A"},${sigcap[i].carrierName ? sigcap[i].carrierName : "N/A"},${sigcap[i].uuid ? sigcap[i].uuid : "N/A"},${sigcap[i].deviceName ? sigcap[i].deviceName : "N/A"},${datetimeIso},`
                            + `${sigcap[i].location.latitude},${sigcap[i].location.longitude},${sigcap[i].location.altitude},${sigcap[i].location.hor_acc},${sigcap[i].location.ver_acc},${dataUtils.getServiceState(sigcap[i], "nrStatus")},${dataUtils.getServiceState(sigcap[i], "nrAvailable")},`
                            + `${dataUtils.getServiceState(sigcap[i], "dcNrRestricted")},${dataUtils.getServiceState(sigcap[i], "enDcAvailable")},${dataUtils.getServiceState(sigcap[i], "nrFrequencyRange")},`
                            + `${dataUtils.getServiceState(sigcap[i], "ci")},"${dataUtils.getServiceState(sigcap[i], "usingCA")}","${dataUtils.getServiceState(sigcap[i], "cellBandwidths")}",${dataUtils.sumCellBw(sigcap[i])},lte,`;

                // Write PCI-Band-EARFCN, bandwidth, rsrp, rsrq, rssi
                entry += `${sigcap[i].cell_info[j].pci},${sigcap[i].cell_info[j].band},${sigcap[i].cell_info[j].earfcn},${sigcap[i].cell_info[j].width},`;
                entry += `${dataUtils.cleanSignal(sigcap[i].cell_info[j].rsrp)},`
                        + `${dataUtils.cleanSignal(sigcap[i].cell_info[j].rsrq)},`
                        + `${dataUtils.cleanSignal(sigcap[i].cell_info[j].rssi)},`;
                // Write primary/other
                entry += `${dataUtils.isLtePrimary(sigcap[i].cell_info[j]) ? "primary" : "other"},`
                // EOL and flush
                entry += `\n`;
                os.write(entry);

                // Clear this entry
                sigcap[i].cell_info.splice(j, 1);
                break;
            }
        }

        // Get NR
        if (sigcap[i].nr_info) {
            for(let j = 0, length2 = sigcap[i].nr_info.length; j < length2; j++){

                let entry = `${op},${sigcap[i].simName ? sigcap[i].simName : "N/A"},${sigcap[i].carrierName ? sigcap[i].carrierName : "N/A"},${sigcap[i].uuid ? sigcap[i].uuid : "N/A"},${sigcap[i].deviceName ? sigcap[i].deviceName : "N/A"},${datetimeIso},`
                            + `${sigcap[i].location.latitude},${sigcap[i].location.longitude},${sigcap[i].location.altitude},${sigcap[i].location.hor_acc},${sigcap[i].location.ver_acc},${dataUtils.getServiceState(sigcap[i], "nrStatus")},${dataUtils.getServiceState(sigcap[i], "nrAvailable")},`
                            + `${dataUtils.getServiceState(sigcap[i], "dcNrRestricted")},${dataUtils.getServiceState(sigcap[i], "enDcAvailable")},${dataUtils.getServiceState(sigcap[i], "nrFrequencyRange")},`
                            + `${dataUtils.getServiceState(sigcap[i], "ci")},"${dataUtils.getServiceState(sigcap[i], "usingCA")}","${dataUtils.getServiceState(sigcap[i], "cellBandwidths")}",${dataUtils.sumCellBw(sigcap[i])},nr,`;
                // Write PCI-Band-EARFCN, bandwidth, rsrp, rsrq, rssi
                entry += `${sigcap[i].nr_info[j].pciNr ? sigcap[i].nr_info[j].pciNr : ""},${sigcap[i].nr_info[j].band ? sigcap[i].nr_info[j].band : ""},`
                        + `${sigcap[i].nr_info[j].nrarfcn ? sigcap[i].nr_info[j].nrarfcn : ""},${sigcap[i].nr_info[j].width ? sigcap[i].nr_info[j].width : ""},`;
                entry += `${dataUtils.cleanSignal(sigcap[i].nr_info[j].ssRsrp)},`
                        + `${dataUtils.cleanSignal(sigcap[i].nr_info[j].ssRsrq)},`
                        + `NaN,`;
                // Write primary/other
                entry += `other,`
                // EOL and flush
                entry += `\n`;
                os.write(entry);
            }
        }

        // Get rest of LTE
        for(let j = 0, length2 = sigcap[i].cell_info.length; j < length2; j++){
            // Write overview
            let entry = `${op},${sigcap[i].simName ? sigcap[i].simName : "N/A"},${sigcap[i].carrierName ? sigcap[i].carrierName : "N/A"},${sigcap[i].uuid ? sigcap[i].uuid : "N/A"},${sigcap[i].deviceName ? sigcap[i].deviceName : "N/A"},${datetimeIso},`
                        + `${sigcap[i].location.latitude},${sigcap[i].location.longitude},${sigcap[i].location.altitude},${sigcap[i].location.hor_acc},${sigcap[i].location.ver_acc},${dataUtils.getServiceState(sigcap[i], "nrStatus")},${dataUtils.getServiceState(sigcap[i], "nrAvailable")},`
                        + `${dataUtils.getServiceState(sigcap[i], "dcNrRestricted")},${dataUtils.getServiceState(sigcap[i], "enDcAvailable")},${dataUtils.getServiceState(sigcap[i], "nrFrequencyRange")},`
                        + `${dataUtils.getServiceState(sigcap[i], "ci")},"${dataUtils.getServiceState(sigcap[i], "usingCA")}","${dataUtils.getServiceState(sigcap[i], "cellBandwidths")}",${dataUtils.sumCellBw(sigcap[i])},lte,`;

            // Write PCI-Band-EARFCN, bandwidth, rsrp, rsrq, rssi
            entry += `${sigcap[i].cell_info[j].pci},${sigcap[i].cell_info[j].band},${sigcap[i].cell_info[j].earfcn},${sigcap[i].cell_info[j].width},`;
            entry += `${dataUtils.cleanSignal(sigcap[i].cell_info[j].rsrp)},`
                    + `${dataUtils.cleanSignal(sigcap[i].cell_info[j].rsrq)},`
                    + `${dataUtils.cleanSignal(sigcap[i].cell_info[j].rssi)},`;
            // Write primary/other
            entry += `${dataUtils.isLtePrimary(sigcap[i].cell_info[j]) ? "primary" : "other"},`
            // EOL and flush
            entry += `\n`;
            os.write(entry);
        }

    }
});

os.close();
