if (!exist('inputList')) {
    set print "-"
    print "inputList variable is needed!"
    exit
}

if (exist('mode') && mode eq 'EPS') {
    if (exist('pdfsizex') && exist('pdfsizey')) {
        sizex = pdfsizex
        sizey = pdfsizey
    } else {
        sizex = 5.5
        sizey = 3.5
    }
    set terminal pdf enhanced color font 'Helvetica,18' size sizex, sizey
    set output outputName.'.pdf'
} else {
    set terminal pngcairo dashed transparent enhanced font 'Helvetica,18' fontscale 0.8 size 800, 600
    set output outputName.'.png'
}

inputs(n) = word(inputList, n)
# titles(n) = word(titleList, n)

#styles
set border 2 front lt black linewidth 1.000 dashtype solid
set boxwidth 0.5 absolute
set style fill solid 0.50 border lt -1
unset key
set pointsize 0.5
set style data boxplot

set xlabel 'Elapsed time (s)'
# set key top right
# set yrange[0:1]
set grid xtics ytics front
# set y2tics 0, 5
# set ytics nomirror

plot for [i=1:words(inputList)] inputs(i) using (i*20):2 lt 1

exit