if (!exist('inputList') || !exist('titleList')) {
    set print "-"
    print "inputList and titleList variable is needed!"
    exit
}

if (exist('mode') && mode eq 'EPS') {
    if (exist('pdfsizex') && exist('pdfsizey')) {
        sizex = pdfsizex
        sizey = pdfsizey
    } else {
        sizex = 5.5
        sizey = 3.5
    }
    set terminal pdf enhanced color font 'Helvetica,18' size sizex, sizey
    set output outputName.'.pdf'
} else {
    set terminal pngcairo dashed transparent enhanced font 'Helvetica,18' fontscale 0.8 size 800, 600
    set output outputName.'.png'
}

inputs(n) = word(inputList, n)
titles(n) = word(titleList, n)

#styles
set style line 1 lw 4
set style line 2 lw 4
set style line 3 lw 4
set style line 4 lw 4
set style line 5 lw 4
set style line 6 lw 4
set style line 7 lw 4
set style line 8 lw 4
set style line 9 lw 4
set style line 10 lw 4
set style line 11 lw 4
set style line 12 lw 4
set style line 13 lw 4

set xlabel 'Elapsed time (s)'
# set key top right
# set yrange[0:1]
set grid xtics ytics front
# set y2tics 0, 5
# set ytics nomirror

plot \
    for [i=1:words(inputList)] inputs(i) using 1:2 title titles(i) axis x1y1 with linespoint ls i-(floor((i-1)/13)*13)

exit