#!/bin/bash

mkdir -p figs
rm figs/*

gnuplot -e \
"inputList='data/p5/cdf-skin-sw41.dat \
data/p5/cdf-skin-sw10.dat \
data/p5/cdf-skin-4.dat \
data/p5/cdf-cpu-sw41.dat \
data/p5/cdf-cpu-sw10.dat \
data/p5/cdf-cpu-4.dat'" -e \
"titleList='\"Skin - NR 4 Ch. to 1 Ch.\" \
\"Skin - NR 1 Ch. to LTE\" \
\"Skin - NR 4 Ch. only\" \
\"CPU - NR 4 Ch. to 1 Ch.\" \
\"CPU - NR 1 Ch. to LTE\" \
\"CPU - NR 4 Ch. only\"'" \
-e "mode='EPS'" \
-e "splitDt=3" \
-e "outputName='figs/003-1-cdf-temps-p5'" \
-e "set key Left above vertical reverse nobox maxrows 3" \
-e "set xrange [20:85]" \
-e "set xlabel 'Temperature (^{o}C)'" \
-e "set title ''" \
../scripts/cdf.gp
