#!/bin/bash

mkdir -p figs
rm figs/*

gnuplot -e \
"inputList='data/ice-1.dat \
data/ice-2.dat \
data/no-ice.dat'" -e \
"titleList='\"Ice - Run 1\" \
\"Ice - Run 2\"
\"No Ice\"'" \
-e "mode='EPS'" \
-e "tputNum=1" \
-e "pdfsizex=5.5" \
-e "pdfsizey=3" \
-e "outputName='figs/ext-1-ice-exp'" \
-e "set key bottom left" \
-e "set ylabel 'Throughput (Mbps)'" \
-e "set title ''" \
../scripts/time.gp

gnuplot -e \
"inputList='data/ice-1.dat \
data/no-ice.dat'" -e \
"titleList='\"Ice\" \
\"No Ice\"'" \
-e "mode='EPS'" \
-e "tputNum=1" \
-e "pdfsizex=5.5" \
-e "pdfsizey=3" \
-e "outputName='figs/ext-1-ice-exp-v2'" \
-e "set key bottom left" \
-e "set ylabel 'Throughput (Mbps)'" \
-e "set title ''" \
../scripts/time.gp