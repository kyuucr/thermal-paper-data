#!/bin/bash

rm figs/*

# -e "set xrange [0:900]" \
# -e "set yrange [0:2000]" \
gnuplot -e \
"inputList='data/p5/all-dlMbps-cpuTemp-nr.dat \
data/p5/all-dlMbps-cpuTemp-lte.dat'" -e \
"titleList='NR \
LTE'" \
-e "outputName='figs/001-1-tput-cpuTemp-p5'" \
-e "set key top right" \
-e "set xlabel 'APP Throughput (Mbps)'" \
-e "set ylabel 'CPU Temperature (°C)'" \
-e "set title 'P5 - FCC ST + BG DL'" \
../scripts/scatter2d.gp

gnuplot -e \
"inputList='data/p5/all-dlMbps-skinTemp-nr.dat \
data/p5/all-dlMbps-skinTemp-lte.dat'" -e \
"titleList='NR \
LTE'" \
-e "outputName='figs/001-2-tput-skinTemp-p5'" \
-e "set key top right" \
-e "set xlabel 'APP Throughput (Mbps)'" \
-e "set ylabel 'Skin Temperature (°C)'" \
-e "set title 'P5 - FCC ST + BG DL'" \
../scripts/scatter2d.gp

gnuplot -e \
"inputList='data/s21/all-dlMbps-cpuTemp-nr.dat \
data/s21/all-dlMbps-cpuTemp-lte.dat'" -e \
"titleList='NR \
LTE'" \
-e "outputName='figs/001-3-tput-cpuTemp-s21'" \
-e "set key bottom right" \
-e "set xlabel 'APP Throughput (Mbps)'" \
-e "set ylabel 'CPU Temperature (°C)'" \
-e "set title 'S21 - Varying combinations of FCC ST and BG DL'" \
../scripts/scatter2d.gp

gnuplot -e \
"inputList='data/s21/all-dlMbps-skinTemp-nr.dat \
data/s21/all-dlMbps-skinTemp-lte.dat'" -e \
"titleList='NR \
LTE'" \
-e "outputName='figs/001-4-tput-skinTemp-s21'" \
-e "set key bottom right" \
-e "set xlabel 'APP Throughput (Mbps)'" \
-e "set ylabel 'Skin Temperature (°C)'" \
-e "set title 'S21 - Varying combinations of FCC ST and BG DL'" \
../scripts/scatter2d.gp

gnuplot -e \
"inputList='data/s21/fccStOnly-dlMbps-skinTemp-nr.dat \
data/s21/fccStOnly-dlMbps-skinTemp-lte.dat'" -e \
"titleList='NR \
LTE'" \
-e "outputName='figs/001-5-tput-skinTemp-s21-fccst'" \
-e "set key bottom right" \
-e "set xlabel 'APP Throughput (Mbps)'" \
-e "set ylabel 'Skin Temperature (°C)'" \
-e "set title 'S21 - FCC ST only'" \
../scripts/scatter2d.gp

gnuplot -e \
"inputList='data/s21/hasBgDl-dlMbps-skinTemp-nr.dat \
data/s21/hasBgDl-dlMbps-skinTemp-lte.dat'" -e \
"titleList='NR \
LTE'" \
-e "outputName='figs/001-6-tput-skinTemp-s21-bgdl'" \
-e "set key bottom right" \
-e "set xlabel 'APP Throughput (Mbps)'" \
-e "set ylabel 'Skin Temperature (°C)'" \
-e "set title 'S21 - FCC ST + BG DL'" \
../scripts/scatter2d.gp

gnuplot -e \
"inputList='data/s21/summer-dlMbps-skinTemp-nr.dat \
data/s21/summer-dlMbps-skinTemp-lte.dat'" -e \
"titleList='NR \
LTE'" \
-e "outputName='figs/001-7-tput-skinTemp-s21-summer'" \
-e "set key bottom right" \
-e "set xlabel 'APP Throughput (Mbps)'" \
-e "set ylabel 'Skin Temperature (°C)'" \
-e "set title 'S21 - Summer (Sep-Oct)'" \
../scripts/scatter2d.gp

gnuplot -e \
"inputList='data/s21/winter-dlMbps-skinTemp-nr.dat \
data/s21/winter-dlMbps-skinTemp-lte.dat'" -e \
"titleList='NR \
LTE'" \
-e "outputName='figs/001-8-tput-skinTemp-s21-winter'" \
-e "set key bottom right" \
-e "set xlabel 'APP Throughput (Mbps)'" \
-e "set ylabel 'Skin Temperature (°C)'" \
-e "set title 'S21 - Winter (Jan)'" \
../scripts/scatter2d.gp

gnuplot -e \
"inputList='data/p5/summer-dlMbps-skinTemp-nr.dat \
data/p5/summer-dlMbps-skinTemp-lte.dat'" -e \
"titleList='NR \
LTE'" \
-e "outputName='figs/001-9-tput-skinTemp-p5-summer'" \
-e "set key bottom right" \
-e "set xlabel 'APP Throughput (Mbps)'" \
-e "set ylabel 'Skin Temperature (°C)'" \
-e "set title 'P5 - Summer (Sep-Oct)'" \
../scripts/scatter2d.gp

gnuplot -e \
"inputList='data/p5/winter-dlMbps-skinTemp-nr.dat \
data/p5/winter-dlMbps-skinTemp-lte.dat'" -e \
"titleList='NR \
LTE'" \
-e "outputName='figs/001-10-tput-skinTemp-p5-winter'" \
-e "set key bottom right" \
-e "set xlabel 'APP Throughput (Mbps)'" \
-e "set ylabel 'Skin Temperature (°C)'" \
-e "set title 'P5 - Winter (Jan)'" \
../scripts/scatter2d.gp

gnuplot -e \
"inputList='data/p5/summer-dlMbps-skinTemp-nr.dat \
data/p5/winter-dlMbps-skinTemp-nr.dat \
data/p5/summer-dlMbps-skinTemp-nr-linefit.dat \
data/p5/winter-dlMbps-skinTemp-nr-linefit.dat \
data/p5/hasBgDl-dlMbps-skinTemp-nr-linefit.dat'" -e \
"titleList='Summer \
Winter \
Summer-LineFit \
Winter-LineFit \
All-LineFit'" \
-e "mode='EPS'" \
-e "numScatter=2" \
-e "outputName='figs/001-11-tput-skinTemp-p5-summerwinter'" \
-e "set key bottom right" \
-e "set xlabel 'APP Throughput (Mbps)'" \
-e "set ylabel 'Skin Temperature (°C)'" \
../scripts/scatter2d-with-line.gp

gnuplot -e \
"inputList='data/p5/chi-summer-dlMbps-skinTemp-nr.dat \
data/p5/chi-winter-dlMbps-skinTemp-nr.dat \
data/p5/chi-summer-dlMbps-skinTemp-nr-linefit.dat \
data/p5/chi-winter-dlMbps-skinTemp-nr-linefit.dat \
data/p5/chi-hasBgDl-dlMbps-skinTemp-nr-linefit.dat'" -e \
"titleList='Summer \
Winter \
Summer-LineFit \
Winter-LineFit \
All-LineFit'" \
-e "mode='EPS'" \
-e "numScatter=2" \
-e "outputName='figs/001-12-tput-skinTemp-p5-summerwinter-chicago'" \
-e "set key bottom right" \
-e "set xlabel 'APP Throughput (Mbps)'" \
-e "set ylabel 'Skin Temperature (°C)'" \
../scripts/scatter2d-with-line.gp
