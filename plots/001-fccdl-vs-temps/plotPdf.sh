#!/bin/bash

rm figs/*

gnuplot -e \
"inputList='data/p5/hasBgDl-dlMbps-skinTemp-nr.dat \
data/p5/hasBgDl-dlMbps-skinTemp-nr-linefit.dat'" -e \
"titleList='\"NR\" \
\"Line fitting\"'" \
-e "mode='EPS'" \
-e "pdfsizex=5.5" \
-e "pdfsizey=3" \
-e "numScatter=1" \
-e "outputName='figs/001-11-tput-skinTemp-p5-summerwinter'" \
-e "set key Left above vertical reverse maxrows 1" \
-e "set xlabel 'APP Throughput (Mbps)'" \
-e "set ylabel 'Skin Temperature (°C)'" \
-e "set title ''" \
../scripts/scatter2d-with-line.gp

gnuplot -e \
"inputList='data/s21/fccStOnly-dlMbps-skinTemp-nr.dat \
data/s21/fccStOnly-dlMbps-skinTemp-nr-linefit.dat'" -e \
"titleList='\"NR\" \
\"Line fitting\"'" \
-e "mode='EPS'" \
-e "pdfsizex=5.5" \
-e "pdfsizey=3" \
-e "numScatter=1" \
-e "outputName='figs/001-12-tput-skinTemp-s21-summer-fccOnly'" \
-e "set key Left above vertical reverse maxrows 1" \
-e "set xlabel 'APP Throughput (Mbps)'" \
-e "set ylabel 'Skin Temperature (°C)'" \
-e "set title ''" \
../scripts/scatter2d-with-line.gp

gnuplot -e \
"inputList='data/s21/all-dlMbps-skinTemp-nr.dat \
data/s21/all-dlMbps-skinTemp-nr-linefit.dat'" -e \
"titleList='\"NR\" \
\"Line fitting\"'" \
-e "mode='EPS'" \
-e "pdfsizex=5.5" \
-e "pdfsizey=3" \
-e "numScatter=1" \
-e "outputName='figs/001-13-tput-skinTemp-s21-summerwinter'" \
-e "set key Left above vertical reverse maxrows 1" \
-e "set xlabel 'APP Throughput (Mbps)'" \
-e "set ylabel 'Skin Temperature (°C)'" \
-e "set title ''" \
../scripts/scatter2d-with-line.gp