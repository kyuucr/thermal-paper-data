#!/bin/bash

mkdir -p figs
rm figs/*

# -e "set xrange [0:900]" \
gnuplot -e \
"inputList='data/p5/10-09-p5b/0-cpuTemp.dat \
data/p5/10-09-p5b/0-gpuTemp.dat \
data/p5/10-09-p5b/0-skinTemp.dat \
data/p5/10-09-p5b/0-dlMbps-all.dat \
data/p5/10-09-p5b/0-dlMbps-nr.dat \
data/p5/10-09-p5b/0-dlMbps-lte.dat'" -e \
"titleList='\"CPU Temp\" \
\"GPU Temp\" \
\"Skin Temp\" \
\"PHY Tput\" \
\"PHY Tput - NR\" \
\"PHY Tput - LTE\"'" \
-e "mode='EPS'" \
-e "tputNum=3" \
-e "outputName='figs/002-1-tput-temp-chicago'" \
-e "set key Left above vertical reverse nobox maxrows 3" \
-e "set yrange [200:1800]" \
-e "set y2range [10:90]" \
-e "set ytics 2000/10" \
-e "set y2tics 100/10" \
-e "set ylabel 'Throughput (Mbps)'" \
-e "set y2label 'Temperature (°C)'" \
-e "set title ''" \
../scripts/time-tput-temp.gp

gnuplot -e \
"inputList='data/p5/10-09-p5b/0-dlMbps-all.dat \
data/p5/10-09-p5b/0-nrCh.dat'" -e \
"titleList='\"PHY Tput\" \
\"NR Channels\"'" \
-e "mode='EPS'" \
-e "tputNum=1" \
-e "pdfsizex=5.5" \
-e "pdfsizey=2" \
-e "outputName='figs/002-2-tput-temp-chicago'" \
-e "set key top right" \
-e "set yrange [0:2000]" \
-e "set y2range [-0.5:4.5]" \
-e "set ytics 200,400" \
-e "set y2tics 0, 1" \
-e "set ylabel 'Throughput (Mbps)'" \
-e "set y2label '# of NR Channels'" \
-e "set title ''" \
../scripts/time-tput-ch.gp

gnuplot -e \
"inputList='data/p5/10-09-p5b/1-cpuTemp.dat \
data/p5/10-09-p5b/1-gpuTemp.dat \
data/p5/10-09-p5b/1-skinTemp.dat \
data/p5/10-09-p5b/1-dlMbps-all.dat \
data/p5/10-09-p5b/1-dlMbps-nr.dat \
data/p5/10-09-p5b/1-dlMbps-lte.dat'" -e \
"titleList='\"CPU Temp\" \
\"GPU Temp\" \
\"Skin Temp\" \
\"PHY Tput\" \
\"PHY Tput - NR\" \
\"PHY Tput - LTE\"'" \
-e "mode='EPS'" \
-e "tputNum=3" \
-e "outputName='figs/002-3-tput-temp-chicago'" \
-e "set key Left above vertical reverse nobox maxrows 3" \
-e "set xrange [200:800]" \
-e "set yrange [0:2000]" \
-e "set y2range [0:100]" \
-e "set ytics 200" \
-e "set y2tics 10" \
-e "set ylabel 'Throughput (Mbps)'" \
-e "set y2label 'Temperature (°C)'" \
-e "set title ''" \
../scripts/time-tput-temp.gp

gnuplot -e \
"inputList='data/p5/10-09-p5b/1-dlMbps-all.dat \
data/p5/10-09-p5b/1-nrCh.dat'" -e \
"titleList='\"PHY Tput\" \
\"NR Channels\"'" \
-e "mode='EPS'" \
-e "tputNum=1" \
-e "pdfsizex=5.5" \
-e "pdfsizey=2" \
-e "outputName='figs/002-4-tput-nrCh-chicago'" \
-e "set key top right" \
-e "set xrange [200:800]" \
-e "set yrange [0:2000]" \
-e "set y2range [-0.5:4.5]" \
-e "set ytics 200,400" \
-e "set y2tics 0, 1" \
-e "set ylabel 'Throughput (Mbps)'" \
-e "set y2label '# of NR Channels'" \
-e "set title ''" \
../scripts/time-tput-ch.gp

gnuplot -e \
"inputList='data/p5/summer-chi-dlMbps-nr.dat \
data/p5/mia-dlMbps-nr.dat \
data/p5/sf-dlMbps-nr.dat'" \
-e "titleList='Chicago \
Miami \
SF'" \
-e "mode='EPS'" \
-e "pdfsizex=5.5" \
-e "pdfsizey=3" \
-e "outputName='figs/002-5-normalized-time-phytput-chicago-miami'" \
-e "set key top right" \
-e "set xlabel 'Elapsed time (s)'" \
-e "set ylabel 'PHY Throughput (Mbps)'" \
-e "set title ''" \
../scripts/scatter2d.gp

gnuplot -e \
"inputList='data/p5/summer-chi-fccDlMbps-nr.dat \
data/p5/winter-chi-fccDlMbps-nr.dat'" \
-e "titleList='Summer \
Winter'" \
-e "mode='EPS'" \
-e "pdfsizex=5.5" \
-e "pdfsizey=3" \
-e "outputName='figs/002-6-normalized-time-fcctput-chicago-summerwinter'" \
-e "set key top right" \
-e "set xlabel 'Elapsed time (s)'" \
-e "set ylabel 'APP Throughput (Mbps)'" \
-e "set title ''" \
../scripts/scatter2d.gp

gnuplot -e \
"inputList='data/p5/cover1-mia-dlMbps-nr.dat \
data/p5/cover0-mia-dlMbps-nr.dat'" \
-e "titleList='\"Covered\" \
\"Uncovered\"'" \
-e "mode='EPS'" \
-e "pdfsizex=5.5" \
-e "pdfsizey=3" \
-e "outputName='figs/002-7-normalized-time-nsgtput-coverexp'" \
-e "set key top right" \
-e "set xlabel 'Elapsed time (s)'" \
-e "set ylabel 'PHY Throughput (Mbps)'" \
-e "set title ''" \
../scripts/scatter2d.gp

gnuplot -e \
"inputList='data/p5/cover1-mia-skinTemp.dat \
data/p5/cover0-mia-skinTemp.dat'" \
-e "titleList='\"Covered\" \
\"Uncovered\"'" \
-e "mode='EPS'" \
-e "pdfsizex=5.5" \
-e "pdfsizey=3" \
-e "outputName='figs/002-8-normalized-time-skintemp-coverexp'" \
-e "set key bottom right" \
-e "set xlabel 'Elapsed time (s)'" \
-e "set ylabel 'Skin Temperature (°C)'" \
-e "set title ''" \
../scripts/scatter2d.gp

gnuplot -e \
"inputList='data/p5/09-29/0-dlMbps-nr-normalized.dat \
data/p5/09-29/1-dlMbps-nr-normalized.dat \
data/p5/09-29/2-dlMbps-nr-normalized.dat \
data/p5/09-29/3-dlMbps-nr-normalized.dat \
data/p5/09-29/4-dlMbps-nr-normalized.dat \
data/p5/09-29/5-dlMbps-nr-normalized.dat'" \
-e "titleList='\"Covered #1\" \
\"Covered #2\" \
\"Covered #3\" \
\"Uncovered #1\" \
\"Uncovered #2\" \
\"Uncovered #3\"'" \
-e "set style line 1 lc '#009E72' dt 1" \
-e "set style line 2 lc '#009E72' dt 1" \
-e "set style line 3 lc '#009E72' dt 1" \
-e "set style line 4 lc '#9300D3' dt 1" \
-e "set style line 5 lc '#9300D3' dt 1" \
-e "set style line 6 lc '#9300D3' dt 1" \
-e "mode='EPS'" \
-e "pdfsizex=5.5" \
-e "pdfsizey=3" \
-e "outputName='figs/002-9-normalized-time-nsgtput-coverexp-v2'" \
-e "set key top right" \
-e "set xlabel 'Elapsed time (s)'" \
-e "set ylabel 'PHY Throughput (Mbps)'" \
-e "set title ''" \
../scripts/time.gp

gnuplot -e \
"inputList='data/p5/09-29/0-skinTemp-normalized.dat \
data/p5/09-29/1-skinTemp-normalized.dat \
data/p5/09-29/2-skinTemp-normalized.dat \
data/p5/09-29/3-skinTemp-normalized.dat \
data/p5/09-29/4-skinTemp-normalized.dat \
data/p5/09-29/5-skinTemp-normalized.dat'" \
-e "titleList='\"Covered #1\" \
\"Covered #2\" \
\"Covered #3\" \
\"Uncovered #1\" \
\"Uncovered #2\" \
\"Uncovered #3\"'" \
-e "set style line 1 lc '#009E72' dt 1" \
-e "set style line 2 lc '#009E72' dt 1" \
-e "set style line 3 lc '#009E72' dt 1" \
-e "set style line 4 lc '#9300D3' dt 1" \
-e "set style line 5 lc '#9300D3' dt 1" \
-e "set style line 6 lc '#9300D3' dt 1" \
-e "mode='EPS'" \
-e "pdfsizex=5.5" \
-e "pdfsizey=3" \
-e "outputName='figs/002-10-normalized-time-skintemp-coverexp-v2'" \
-e "set key bottom right" \
-e "set xlabel 'Elapsed time (s)'" \
-e "set ylabel 'Skin Temperature (°C)'" \
-e "set title ''" \
../scripts/time.gp

