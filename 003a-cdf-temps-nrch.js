const agg = require('./lib/aggregator');
const commandParser = require('./lib/command-parser');
const csvUtils = require('./lib/csv-utils');
const dataUtils = require('./lib/data-utils');
const statsUtils = require('./lib/stats-utils');
const fs = require('fs');
const path = require('path');


// Fn & var declarations
const fileWritten = {};
const writeToFile = function(path, data, header, isAppend = false) {
    if (isAppend && fileWritten[path] === true) {
        fs.appendFileSync(path, data);
    } else {
        if (isAppend) {
            fileWritten[path] = true;
        }
        let fullData = (header ? header : "") + data;
        fs.writeFileSync(path, fullData);
    }
}

const headerCpuTemp = `"Cumulative distribution function" "CPU Temp (°C)"\n`;
const headerGpuTemp = `"Cumulative distribution function" "GPU Temp (°C)"\n`;
const headerSkinTemp = `"Cumulative distribution function" "Skin Temp (°C)"\n`;
const bucket = {};
const testIdBin = [];


// Process arguments
commandParser.addArgument("input dir", { defaultValue: "./input" });
commandParser.addArgument("output dir", { defaultValue: "./plots/003-cdf-temps-nrch/data" });
// commandParser.addArgument("time bucket", { keyword: "--time-bucket", defaultValue: 1000 });
const options = commandParser.parse();

// time bucket in seconds
// let timeBucket = parseFloat(options["time bucket"]) / 1000;
const cdf = {
    p5: {
        cpu: { 0: [], 1: [], 4: [], sw01: [], sw14: [], sw41: [], sw10: [] },
        skin: { 0: [], 1: [], 4: [], sw01: [], sw14: [], sw41: [], sw10: [] }
    },
    s21: {
        cpu: { 0: [], 1: [], 4: [], sw01: [], sw14: [], sw41: [], sw10: [] },
        skin: { 0: [], 1: [], 4: [], sw01: [], sw14: [], sw41: [], sw10: [] }
    }
};

const metadataList = agg.getFileListRecursive(options["input dir"], /metadata-.*.json$/);
metadataList.forEach(metadataFile => {

    console.log(`Processing... ${metadataFile}`);
    const metadata = JSON.parse(fs.readFileSync(metadataFile));
    const inputDir = metadataFile.replace(new RegExp(`${path.sep}metadata-|\\.json$`, "g"), "/");

    // Process timespan
    let nsgData = {};
    let timespans = metadata.timespans;
    for (let i = 0, length1 = timespans.length; i < length1; i++) {
        timespans[i].start = new Date(timespans[i].start).getTime();
        timespans[i].end = new Date(timespans[i].end).getTime();

        // Check for nsg csv data on each timespan and read it
        if (timespans[i].nsg !== undefined) {
            console.log(`Reading NSG file... ${path.join(inputDir, timespans[i].nsg)}`);
            let nsgCsv = agg.getCsv(path.join(inputDir, timespans[i].nsg));
            let header = nsgCsv.headerObj;
            nsgCsv.csv.forEach(row => {
                let time = parseInt(row[header["Elapsed Time (s)"]]);
                let nrCh = parseInt(row[header["# of NR channel"]]);

                if (nsgData[i] === undefined) {
                    nsgData[i] = {};
                }
                nsgData[i][time] = nrCh;
            });
        }
    }
    // console.log(nsgData)
    const nsgKeys = Object.keys(nsgData);
    if (nsgKeys.length === 0) {
        // No nsg data, skip this file
        return;
    }

    // Skip FCC ST only data
    if (metadata.description.match(/FCC ST Only/i) !== null) {
        return;
    }

    const isWinter = path.basename(inputDir).startsWith("01");
    const isChicago = metadataFile.match(/09-2(8|9)/) === null;
    let pathSplit = inputDir.split(path.sep);
    const outputDir = path.join(options["output dir"], pathSplit[1], pathSplit[2]);
    const prefix = pathSplit[1];
    console.log(`is winter: ${isWinter}; is Chicago: ${isChicago}`);
    console.log(`Desc.: ${metadata.description}`);
    console.log(`Input dir: ${inputDir}`);
    console.log(`Output dir: ${outputDir}`);

    const testIdBin = [];

    nsgKeys.forEach(idx => {
        bucket[idx] = {};
    });

    let tempsData = {}, fccFilter = {};

    agg.callbackJsonRecursive(inputDir, data => {
        let json = data.json;
        let dataType = dataUtils.getJSONType(json[0]);

        if (dataType === "sigcap") {
            console.log(`Folder: ${data.path}; Data type: ${dataType}; # of data: ${json.length}`);
            for (let i = 0, length1 = json.length; i < length1; i++){
                let time = new Date(json[i].datetimeIso).getTime();
                let idx;
                for (let j = 0, length2 = nsgKeys.length; j < length2; j++) {
                    if (time > timespans[nsgKeys[j]].start && time < timespans[nsgKeys[j]].end) {
                        idx = nsgKeys[j];
                        break;
                    }
                }
                if (idx === undefined) {
                    continue;
                }

                let timeIdx = Math.floor((time - timespans[idx].start) / 1e3);
                if (tempsData[idx] === undefined) {
                    tempsData[idx] = {}
                }
                tempsData[idx][timeIdx] = { cpuTemp: NaN, skinTemp: NaN };

                if (json[i].sensor && nsgData[idx][timeIdx] !== undefined) {
                    if (json[i].sensor.hardwareCpuTempC && json[i].sensor.hardwareCpuTempC.length > 0) {
                        tempsData[idx][timeIdx].cpuTemp = statsUtils.meanArray(json[i].sensor.hardwareCpuTempC);
                    }
                    if (json[i].sensor.hardwareSkinTempC && json[i].sensor.hardwareSkinTempC.length > 0) {
                        tempsData[idx][timeIdx].skinTemp = statsUtils.meanArray(json[i].sensor.hardwareSkinTempC);
                    }
                }
            }
        }  else if (dataType == "fcc") {
            // Take FCC DL test time for filter start time and end time
            for(let i = 0, length1 = json.length; i < length1; i++) {
                let testId = json[i].tests.testId;
                // Skip duplicates testId
                if (testId === undefined || testIdBin.includes(testId)) continue;
                testIdBin.push(testId);

                // Check if download test exist and not 0
                let downloadTest = json[i].tests.download;
                if (downloadTest === undefined
                        || downloadTest.local_datetime === undefined
                        || downloadTest.throughput === 0) {
                    continue;
                }

                // Get valid time idx
                let time = new Date(downloadTest.local_datetime).getTime();
                let idx;
                for (let j = 0, length2 = nsgKeys.length; j < length2; j++) {
                    if (time > timespans[nsgKeys[j]].start && time < timespans[nsgKeys[j]].end) {
                        idx = nsgKeys[j];
                        break;
                    }
                }
                if (idx === undefined) {
                    continue;
                }
                let timeIdx = Math.floor((time - timespans[idx].start) / 1e3);

                // Put time as filter
                if (fccFilter[idx] === undefined) {
                    fccFilter[idx] = { startTime: timeIdx, endTime: timeIdx };
                } else {
                    if (fccFilter[idx].startTime > timeIdx) {
                        fccFilter[idx].startTime = timeIdx;
                    }
                    if (fccFilter[idx].endTime < timeIdx) {
                        fccFilter[idx].endTime = timeIdx;
                    }
                }
            }
        }
    }); // End of agg.callbackJsonRecursive

    // Create output folder if not exist
    fs.mkdirSync(outputDir, { recursive: true });

    // Correlate temps and nrCh
    let cdfCurr = {};
    for (let idx in nsgData) {
        let prevNrCh = -1, changeEvent = null;

        cdfCurr[idx] = {
            cpu: { 0: [], 1: [], 4: [], sw01: [], sw14: [], sw41: [], sw10: [] },
            skin: { 0: [], 1: [], 4: [], sw01: [], sw14: [], sw41: [], sw10: [] }
        };
        console.log(`IDX ${idx}`);
        for (let timeIdx in nsgData[idx]) {
            // Filter by fcc traffic start - end
            if (fccFilter[idx] !== undefined
                    && (timeIdx < fccFilter[idx].startTime || timeIdx > fccFilter[idx].endTime)) {
                console.log(`Skipped time ${timeIdx}`);
                continue;
            }

            let nrCh = nsgData[idx][timeIdx];
            console.log(`At ${timeIdx}, nrCh: ${nrCh}`);

            // Put NR Channel if there is change in value
            if (prevNrCh !== nrCh) {
                if (prevNrCh >= 0) {
                    console.log(`nrCh changed ${prevNrCh} -> ${nrCh}`);
                    changeEvent = `sw${prevNrCh}${nrCh}`;
                }
            }
            prevNrCh = nrCh;

            // Check tempsData
            if (tempsData[idx][timeIdx] !== undefined) {
                let { cpuTemp, skinTemp } = tempsData[idx][timeIdx];
                console.log(`tempsData found: cpu ${cpuTemp}, skin ${skinTemp}`);

                if (!isNaN(cpuTemp)) {
                    cdfCurr[idx].cpu[nrCh].push(cpuTemp);
                    cdf[prefix].cpu[nrCh].push(cpuTemp);
                }
                if (!isNaN(skinTemp)) {
                    cdfCurr[idx].skin[nrCh].push(skinTemp);
                    cdf[prefix].skin[nrCh].push(skinTemp);
                }

                // Change event
                if (changeEvent) {
                    console.log(`changeEvent consumed: ${changeEvent}`);
                    if (!isNaN(cpuTemp)) {
                        cdfCurr[idx].cpu[changeEvent].push(cpuTemp);
                        cdf[prefix].cpu[changeEvent].push(cpuTemp);
                    }
                    if (!isNaN(skinTemp)) {
                        cdfCurr[idx].skin[changeEvent].push(skinTemp);
                        cdf[prefix].skin[changeEvent].push(skinTemp);
                    }
                    changeEvent = null;
                }
            }
        }
    }

    // Write gnuplot data
    for (let idx in cdfCurr) {
        for (let type in cdfCurr[idx]) {
            for (let ch in cdfCurr[idx][type]) {
                cdfCurr[idx][type][ch].sort(dataUtils.sortNumAsc);
                let stringOut = "";
                for (let i = 0, length1 = cdfCurr[idx][type][ch].length; i < length1; i++) {
                    stringOut += `${i / (length1 - 1)} ${cdfCurr[idx][type][ch][i]}\n`;
                }

                writeToFile(path.join(outputDir, `${idx}-cdf-${type}-${ch}.dat`),
                        stringOut, type === "cpu" ? headerCpuTemp : headerSkinTemp);
            }
        }
    }
});

// Write output by prefix
for (let prefix in cdf) {
    let outputDir = path.join(options["output dir"], prefix);
    // Create output folder if not exist
    fs.mkdirSync(outputDir, { recursive: true });

    for (let type in cdf[prefix]) {
        for (let ch in cdf[prefix][type]) {
            cdf[prefix][type][ch].sort(dataUtils.sortNumAsc);
            let stringOut = "";
            for (let i = 0, length1 = cdf[prefix][type][ch].length; i < length1; i++) {
                stringOut += `${i / (length1 - 1)} ${cdf[prefix][type][ch][i]}\n`;
            }

            writeToFile(path.join(outputDir, `cdf-${type}-${ch}.dat`),
                    stringOut, type === "cpu" ? headerCpuTemp : headerSkinTemp);
        }
    }
}

console.log(`Done!`);
