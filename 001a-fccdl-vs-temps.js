const agg = require('./lib/aggregator');
const commandParser = require('./lib/command-parser');
const csvUtils = require('./lib/csv-utils');
const dataUtils = require('./lib/data-utils');
const statsUtils = require('./lib/stats-utils');
const fs = require('fs');
const path = require('path');


// Fn declarations
const fileWritten = {};
const writeToFile = function(path, data, header, isAppend = false) {
    if (isAppend && fileWritten[path] === true) {
        fs.appendFileSync(path, data);
    } else {
        if (isAppend) {
            fileWritten[path] = true;
        }
        let fullData = (header ? header : "") + data;
        fs.writeFileSync(path, fullData);
    }
}


// Process arguments
commandParser.addArgument("input dir", { defaultValue: "./input" });
commandParser.addArgument("output dir", { defaultValue: "./plots/001-fccdl-vs-temps/data" });
commandParser.addArgument("time bucket", { keyword: "--time-bucket", defaultValue: 5000 });
const options = commandParser.parse();

const sigcapNrRsrp = { p5: [], s21: [] };
const throttle = { p5: { cpuTemp: [], gpuTemp: [], skinTemp: [] }, s21: { cpuTemp: [], gpuTemp: [], skinTemp: [] } };
const metadataList = agg.getFileListRecursive(options["input dir"], /metadata-.*.json$/);
metadataList.forEach(metadataFile => {
    const metadata = JSON.parse(fs.readFileSync(metadataFile));
    const isFccOnly = metadata.description.match(/FCC ST Only/i) !== null;
    const inputDir = metadataFile.replace(new RegExp(`${path.sep}metadata-|\\.json$`, "g"), "/");
    const isWinter = path.basename(inputDir).startsWith("01");
    const isChicago = metadataFile.match(/09-2(8|9)/) === null;
    let pathSplit = inputDir.split(path.sep);
    const outputDir = path.join(options["output dir"], pathSplit[1], pathSplit[2]);
    const prefix = pathSplit[1];
    console.log(`Processing... ${metadataFile}`);
    console.log(`is FCC ST only: ${isFccOnly}; is winter: ${isWinter}; is Chicago: ${isChicago}`);
    console.log(`Desc.: ${metadata.description}`);
    console.log(`Input dir: ${inputDir}`);
    console.log(`Output dir: ${outputDir}`);

    let timespans = metadata.timespans;
    for (let i = 0, length1 = timespans.length; i < length1; i++) {
        timespans[i].start = new Date(timespans[i].start).getTime();
        timespans[i].end = new Date(timespans[i].end).getTime();
    }

    const bucket = {};
    const fccBin = [];

    agg.callbackJsonRecursive(inputDir, data => {
        let json = data.json;
        let dataType = dataUtils.getJSONType(json[0]);
        console.log(`Folder: ${data.path}; Data type: ${dataType}; # of data: ${json.length}`);

        if (dataType === "fcc") {
            // FCC
            for(let i = 0, length1 = json.length; i < length1; i++){
                // Check test id
                let testId = json[i].tests.testId;
                if (testId === undefined || fccBin.includes(testId)) continue;
                fccBin.push(testId);

                // Check if download test exist and not 0
                let downloadTest = json[i].tests.download;
                if (downloadTest === undefined
                        || downloadTest.local_datetime === undefined
                        || downloadTest.throughput === 0) {
                    continue;
                }

                // Check if test time is in the timespans
                let startTime = new Date(downloadTest.local_datetime).getTime();
                let timeFound = timespans.some(timespan =>
                        startTime > timespan.start && startTime < timespan.end);
                if (!timeFound) continue;

                // This will put the data in a 5 second bucket
                let idx = `${Math.floor(startTime / options["time bucket"])}`;
                if (bucket[idx] === undefined) {
                    bucket[idx] = {
                        type: null,
                        dlMbps: null,
                        sigcapNrRsrp: null,
                        cpuTemp: null,
                        gpuTemp: null,
                        skinTemp: null
                    };
                }

                // Check if both start and end of test are the same
                let startNet = dataUtils.getCleanValue(downloadTest, "environment.beginning.network.subtype_name");
                let endNet = dataUtils.getCleanValue(downloadTest, "environment.end.network.subtype_name");
                if (startNet !== endNet) continue;
                // bucket[idx].type = startNet;

                // Take tput value
                bucket[idx].dlMbps = downloadTest.throughput * 8 / 1e6;
            }
        } else if (dataType === "sigcap") {
            // SigCap
            for (let i = 0, length1 = json.length; i < length1; i++){
                // Check if time is in the timespans
                let time = new Date(json[i].datetimeIso).getTime();
                let timeFound = timespans.some(timespan =>
                        time > timespan.start && time < timespan.end);
                if (!timeFound) continue;

                // This will put the data in a 5 second bucket
                let idx = `${Math.floor(time / options["time bucket"])}`;
                if (bucket[idx] === undefined) {
                    bucket[idx] = {
                        type: null,
                        dlMbps: null,
                        sigcapNrRsrp: null,
                        cpuTemp: null,
                        gpuTemp: null,
                        skinTemp: null
                    };
                }

                bucket[idx].type = dataUtils.getNetworkType(json[i]);
                if (json[i].sensor) {
                    if (json[i].sensor.hardwareCpuTempC && json[i].sensor.hardwareCpuTempC.length > 0) {
                        bucket[idx].cpuTemp = statsUtils.meanArray(json[i].sensor.hardwareCpuTempC);
                    }
                    if (json[i].sensor.hardwareGpuTempC && json[i].sensor.hardwareGpuTempC.length > 0) {
                        bucket[idx].gpuTemp = statsUtils.meanArray(json[i].sensor.hardwareGpuTempC);
                    }
                    if (json[i].sensor.hardwareSkinTempC && json[i].sensor.hardwareSkinTempC.length > 0) {
                        bucket[idx].skinTemp = statsUtils.meanArray(json[i].sensor.hardwareSkinTempC);
                    }
                    if (json[i].sensor.hardwareCpuThrotTempC && json[i].sensor.hardwareCpuThrotTempC.length > 0) {
                        throttle[prefix].cpuTemp.push(statsUtils.meanArray(json[i].sensor.hardwareCpuThrotTempC));
                    }
                    if (json[i].sensor.hardwareGpuThrotTempC && json[i].sensor.hardwareGpuThrotTempC.length > 0) {
                        throttle[prefix].gpuTemp.push(statsUtils.meanArray(json[i].sensor.hardwareGpuThrotTempC));
                    }
                    if (json[i].sensor.hardwareSkinThrotTempC && json[i].sensor.hardwareSkinThrotTempC.length > 0) {
                        throttle[prefix].skinTemp.push(statsUtils.meanArray(json[i].sensor.hardwareSkinThrotTempC));
                    }
                }
                if (json[i].nr_info && json[i].nr_info[0] && json[i].nr_info[0].ssRsrp !== dataUtils.INVALID_VAL) {
                    bucket[idx].sigcapNrRsrp = json[i].nr_info[0].ssRsrp;
                }
            }
        }
    }); // End of agg.callbackJsonRecursive

    // Create output folder if not exist
    fs.mkdirSync(outputDir, { recursive: true });

    // Write gnuplot data & set up for linear regression
    let stringCpuNr = "", stringSkinNr = "";
    let stringCpuLte = "", stringSkinLte = "";
    Object.values(bucket).forEach(data => {
        if (data.dlMbps !== null
                && data.cpuTemp !== null
                && data.skinTemp !== null
                && typeof data.type === "string") {
            if (data.type.startsWith("NR")) {
                stringCpuNr += `${data.dlMbps} ${data.cpuTemp}\n`;
                stringSkinNr += `${data.dlMbps} ${data.skinTemp}\n`;
                if (isChicago && !isFccOnly) {
                    sigcapNrRsrp[prefix].push(data.sigcapNrRsrp);
                }
            } else if (data.type === "LTE") {
                stringCpuLte += `${data.dlMbps} ${data.cpuTemp}\n`;
                stringSkinLte += `${data.dlMbps} ${data.skinTemp}\n`;
            }
        }
    });

    let headerCpu = `"FCC ST Throughput (Mbps)" "CPU Temp (°C)"\n`;
    let headerSkin = `"FCC ST Throughput (Mbps)" "Skin Temp (°C)"\n`;
    writeToFile(path.join(outputDir, `dlMbps-cpuTemp-lte.dat`),
            stringCpuLte, headerCpu);
    writeToFile(path.join(outputDir, `dlMbps-cpuTemp-nr.dat`),
            stringCpuNr, headerCpu);
    writeToFile(path.join(outputDir, `dlMbps-skinTemp-lte.dat`),
            stringSkinLte, headerSkin);
    writeToFile(path.join(outputDir, `dlMbps-skinTemp-nr.dat`),
            stringSkinNr, headerSkin);

    // Write gnuplot data on all
    writeToFile(path.join(options["output dir"], prefix, `all-dlMbps-cpuTemp-lte.dat`),
        stringCpuLte, headerCpu, true);
    writeToFile(path.join(options["output dir"], prefix, `all-dlMbps-cpuTemp-nr.dat`),
        stringCpuNr, headerCpu, true);
    writeToFile(path.join(options["output dir"], prefix, `all-dlMbps-skinTemp-lte.dat`),
        stringSkinLte, headerSkin, true);
    writeToFile(path.join(options["output dir"], prefix, `all-dlMbps-skinTemp-nr.dat`),
        stringSkinNr, headerSkin, true);

    // Write gnuplot data on FCC ST only/otherwise
    if (isFccOnly) {
        writeToFile(path.join(options["output dir"], prefix, `fccStOnly-dlMbps-cpuTemp-lte.dat`),
            stringCpuLte, headerCpu, true);
        writeToFile(path.join(options["output dir"], prefix, `fccStOnly-dlMbps-cpuTemp-nr.dat`),
            stringCpuNr, headerCpu, true);
        writeToFile(path.join(options["output dir"], prefix, `fccStOnly-dlMbps-skinTemp-lte.dat`),
            stringSkinLte, headerSkin, true);
        writeToFile(path.join(options["output dir"], prefix, `fccStOnly-dlMbps-skinTemp-nr.dat`),
            stringSkinNr, headerSkin, true);
    } else {
        writeToFile(path.join(options["output dir"], prefix, `hasBgDl-dlMbps-cpuTemp-lte.dat`),
            stringCpuLte, headerCpu, true);
        writeToFile(path.join(options["output dir"], prefix, `hasBgDl-dlMbps-cpuTemp-nr.dat`),
            stringCpuNr, headerCpu, true);
        writeToFile(path.join(options["output dir"], prefix, `hasBgDl-dlMbps-skinTemp-lte.dat`),
            stringSkinLte, headerSkin, true);
        writeToFile(path.join(options["output dir"], prefix, `hasBgDl-dlMbps-skinTemp-nr.dat`),
            stringSkinNr, headerSkin, true);
    }

    // Write gnuplot data on winter/summer
    if (isWinter) {
        writeToFile(path.join(options["output dir"], prefix, `winter-dlMbps-cpuTemp-lte.dat`),
            stringCpuLte, headerCpu, true);
        writeToFile(path.join(options["output dir"], prefix, `winter-dlMbps-cpuTemp-nr.dat`),
            stringCpuNr, headerCpu, true);
        writeToFile(path.join(options["output dir"], prefix, `winter-dlMbps-skinTemp-lte.dat`),
            stringSkinLte, headerSkin, true);
        writeToFile(path.join(options["output dir"], prefix, `winter-dlMbps-skinTemp-nr.dat`),
            stringSkinNr, headerSkin, true);
    } else {
        writeToFile(path.join(options["output dir"], prefix, `summer-dlMbps-cpuTemp-lte.dat`),
            stringCpuLte, headerCpu, true);
        writeToFile(path.join(options["output dir"], prefix, `summer-dlMbps-cpuTemp-nr.dat`),
            stringCpuNr, headerCpu, true);
        writeToFile(path.join(options["output dir"], prefix, `summer-dlMbps-skinTemp-lte.dat`),
            stringSkinLte, headerSkin, true);
        writeToFile(path.join(options["output dir"], prefix, `summer-dlMbps-skinTemp-nr.dat`),
            stringSkinNr, headerSkin, true);
    }

    // Write only Chicago data that uses FCC ST + BG DL traffic
    if (isChicago && !isFccOnly) {
        writeToFile(path.join(options["output dir"], prefix, `chi-hasBgDl-dlMbps-cpuTemp-lte.dat`),
            stringCpuLte, headerCpu, true);
        writeToFile(path.join(options["output dir"], prefix, `chi-hasBgDl-dlMbps-cpuTemp-nr.dat`),
            stringCpuNr, headerCpu, true);
        writeToFile(path.join(options["output dir"], prefix, `chi-hasBgDl-dlMbps-skinTemp-lte.dat`),
            stringSkinLte, headerSkin, true);
        writeToFile(path.join(options["output dir"], prefix, `chi-hasBgDl-dlMbps-skinTemp-nr.dat`),
            stringSkinNr, headerSkin, true);

        // Write gnuplot data on winter/summer
        if (isWinter) {
            writeToFile(path.join(options["output dir"], prefix, `chi-winter-dlMbps-cpuTemp-lte.dat`),
                stringCpuLte, headerCpu, true);
            writeToFile(path.join(options["output dir"], prefix, `chi-winter-dlMbps-cpuTemp-nr.dat`),
                stringCpuNr, headerCpu, true);
            writeToFile(path.join(options["output dir"], prefix, `chi-winter-dlMbps-skinTemp-lte.dat`),
                stringSkinLte, headerSkin, true);
            writeToFile(path.join(options["output dir"], prefix, `chi-winter-dlMbps-skinTemp-nr.dat`),
                stringSkinNr, headerSkin, true);
        } else {
            writeToFile(path.join(options["output dir"], prefix, `chi-summer-dlMbps-cpuTemp-lte.dat`),
                stringCpuLte, headerCpu, true);
            writeToFile(path.join(options["output dir"], prefix, `chi-summer-dlMbps-cpuTemp-nr.dat`),
                stringCpuNr, headerCpu, true);
            writeToFile(path.join(options["output dir"], prefix, `chi-summer-dlMbps-skinTemp-lte.dat`),
                stringSkinLte, headerSkin, true);
            writeToFile(path.join(options["output dir"], prefix, `chi-summer-dlMbps-skinTemp-nr.dat`),
                stringSkinNr, headerSkin, true);
        }
    }
});

Object.keys(sigcapNrRsrp).forEach(key => {
    console.log(`Average NR RSRP on ${key}: ${statsUtils.meanArray(sigcapNrRsrp[key])}`);
    console.log(`Average CPU Throttle Threshold on ${key}: ${statsUtils.meanArray(throttle[key].cpuTemp)}`);
    console.log(`Average GPU Throttle Threshold on ${key}: ${statsUtils.meanArray(throttle[key].gpuTemp)}`);
    console.log(`Average Skin Throttle Threshold on ${key}: ${statsUtils.meanArray(throttle[key].skinTemp)}`);
});
console.log(`Done!`);
