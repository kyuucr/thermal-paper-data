const agg = require('./lib/aggregator');
const commandParser = require('./lib/command-parser');
const csvUtils = require('./lib/csv-utils');
const dataUtils = require('./lib/data-utils');
const statsUtils = require('./lib/stats-utils');
const fs = require('fs');
const path = require('path');


// Fn & var declarations
const fileWritten = {};
const writeToFile = function(path, data, header, isAppend = false) {
    console.log(`Writing to ${path}`)
    if (isAppend && fileWritten[path] === true) {
        fs.appendFileSync(path, data);
    } else {
        if (isAppend) {
            fileWritten[path] = true;
        }
        let fullData = (header ? header : "") + data;
        fs.writeFileSync(path, fullData);
    }
}

const headerTput = `"Elapsed time (s)" "PHY Throughput (Mbps)"\n`;
const headerSkin = `"Elapsed time (s)" "Skin Temp (°C)"\n`;

// Process arguments
commandParser.addArgument("input dir", { defaultValue: "./plots/002-time-vs-nsgdl-temps/data" });
commandParser.addArgument("output dir", { defaultValue: "./plots/002-time-vs-nsgdl-temps/data" });
// commandParser.addArgument("time bucket", { keyword: "--time-bucket", defaultValue: 20000 });
// commandParser.addArgument("# of buckets", { keyword: "--num-bucket", defaultValue: 30 });
const options = commandParser.parse();
// options["time bucket"] = parseInt(options["time bucket"]) / 1000;
// options["# of buckets"] = parseInt(options["# of buckets"]);


const gnuplotList = agg.getFileListRecursive(options["input dir"], /\/\d+-dlMbps-nr\.dat$/);
gnuplotList.forEach(gnuplotFile => {
    let startTime = -1;
    const isWinter = gnuplotFile.match(/01-\d\d/) !== null;
    const isChicago = gnuplotFile.match(/09-2(8|9)/) === null;
    const isMiami = gnuplotFile.match(/09-29/) !== null;
    const isMiamiCovered = isMiami && (gnuplotFile.match(/[0-2]-dlMbps/) !== null);
    const isMiamiUncovered = isMiami && (gnuplotFile.match(/[3-5]-dlMbps/) !== null);
    const isChicagoCovered = gnuplotFile.match(/10-09-p5b\/8-dlMbps/) !== null;
    const isChicagoUncovered = gnuplotFile.match(/10-09-p5r\/8-dlMbps/) !== null;
    const prefix = gnuplotFile.split(path.sep)[3];

    // Init bucket
    const bucket = { "dlMbps-nr": "", skinTemp: "" };

    // dlMbps bucket
    console.log(`Processing... ${gnuplotFile}`);
    let inputLines = fs.readFileSync(gnuplotFile).toString().split("\n");
    if (inputLines[0].includes("time")) {
        inputLines.splice(0, 1);
    }
    inputLines.forEach(line => {
        let split = line.split(" ");
        let time = parseFloat(split[0]);
        let dlMbps = parseFloat(split[1]);
        if (!isNaN(dlMbps) && !isNaN(time)) {
            if (startTime === -1 && dlMbps > 0) {
                startTime = time;
            }
            if (startTime >= 0) {
                let diffTime = (time - startTime);
                if (diffTime >= 0) {
                    bucket["dlMbps-nr"] += `${diffTime} ${dlMbps}\n`;
                }
            }
        }
    });

    if (startTime === -1) {
        console.log(`error at ${gnuplotFile}, cannot find start time`);
        return;
    }

    // skinTemp bucket
    gnuplotFile = gnuplotFile.replace(/-dlMbps-nr\.dat$/, "-skinTemp.dat");
    console.log(`Processing... ${gnuplotFile}`);
    inputLines = fs.readFileSync(gnuplotFile).toString().split("\n");
    if (inputLines[0].includes("time")) {
        inputLines.splice(0, 1);
    }
    inputLines.forEach(line => {
        let split = line.split(" ");
        let time = parseFloat(split[0]);
        let skinTemp = parseFloat(split[1]);
        if (!isNaN(skinTemp) && !isNaN(time) ) {
            let diffTime = (time - startTime);
            if (diffTime >= 0) {
                bucket.skinTemp += `${diffTime} ${skinTemp}\n`;
            }
        }
    });

    // Write it down
    for (let type in bucket) {
        writeToFile(path.join(path.dirname(gnuplotFile),
                        `${path.basename(gnuplotFile).split("-")[0]}-${type}-normalized.dat`),
                        bucket[type], type === "skinTemp" ? headerSkin : headerTput);
        if (isChicago) {
            writeToFile(path.join(options["output dir"], prefix, `chi-${type}.dat`),
                    bucket[type], type === "skinTemp" ? headerSkin : headerTput, true);
            if (isWinter) {
                writeToFile(path.join(options["output dir"], prefix, `winter-chi-${type}.dat`),
                        bucket[type], type === "skinTemp" ? headerSkin : headerTput, true);
            } else {
                writeToFile(path.join(options["output dir"], prefix, `summer-chi-${type}.dat`),
                        bucket[type], type === "skinTemp" ? headerSkin : headerTput, true);
            }
            if (isChicagoCovered) {
                writeToFile(path.join(options["output dir"], prefix, `cover1-chi-${type}.dat`),
                        bucket[type], type === "skinTemp" ? headerSkin : headerTput, true);
            } else if (isChicagoUncovered) {
                writeToFile(path.join(options["output dir"], prefix, `cover0-chi-${type}.dat`),
                        bucket[type], type === "skinTemp" ? headerSkin : headerTput, true);
            }
        } else if (isMiami) {
            writeToFile(path.join(options["output dir"], prefix, `mia-${type}.dat`),
                    bucket[type], type === "skinTemp" ? headerSkin : headerTput, true);
            if (isMiamiCovered) {
                writeToFile(path.join(options["output dir"], prefix, `cover1-mia-${type}.dat`),
                        bucket[type], type === "skinTemp" ? headerSkin : headerTput, true);
            } else if (isMiamiUncovered) {
                writeToFile(path.join(options["output dir"], prefix, `cover0-mia-${type}.dat`),
                        bucket[type], type === "skinTemp" ? headerSkin : headerTput, true);
            }
        } else {
            writeToFile(path.join(options["output dir"], prefix, `sf-${type}.dat`),
                    bucket[type], type === "skinTemp" ? headerSkin : headerTput, true);
        }
    }
});

console.log(`Done!`);
