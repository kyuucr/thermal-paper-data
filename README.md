-----------------
# Thermal Experiments Data
-----------------

This is a data repository for our paper "Impact of Thermal Throttling on 5G mmWave-enabled Devices" which contains data collected using SigCap[1], FCC Speed Test[2], and NSG[3] apps, and scripts used to process it. This project is based on The Map Project[3].

## Scripts Requirements

- Node.js v16.13.0+
- Gnuplot v5.2+

### Usages

- Generate all data and plots
````
npm install
npm run build
````

### Usages

- `input` contains raw input of SigCap and FCC data
- `plots` contains gnuplot data, scripts, and outputs
- `csvs` contains SigCap, FCC, and NSG data in CSV format

------------------------
[1] https://muhiqbalcr.page/SigCap
[2] https://play.google.com/store/apps/details?id=com.samknows.fcc
[3] https://bitbucket.org/kyuucr/the-map-project/src/master/
