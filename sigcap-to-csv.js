const agg = require('./lib/aggregator');
const csvUtils = require('./lib/csv-utils');
const dataUtils = require('./lib/data-utils');
const filter = require('./lib/filter');
const fs = require('fs');
const path = require('path');

let args = process.argv.slice(2);
if (args.length < 1) {
    console.log("Not enough argument!");
    console.log(`Usage: nodejs ${path.basename(__filename)} <input folder> [output path] [--max-lte <max lte cell limit>] [--max-wifi <max wifi AP limit>] [--filter <filter JSON string>] [--include-invalid-op] [--print-sensor-data]`);
    process.exit(1);
}

let lteLimit = -1;
let wifiLimit = -1;
let outputPath = "output-sigcap.csv";
let inputFilter;
let skipInvalidOp = true;
let printSensorData = false;

let inputFolder = args[0];
args.splice(0, 1);
while (args.length > 0) {
    switch (args[0]) {
        case "--max-lte": {
            lteLimit = parseInt(args[1]);
            args.splice(0, 2);
            break;
        }
        case "--max-wifi": {
            wifiLimit = parseInt(args[1]);
            args.splice(0, 2);
            break;
        }
        case "--filter": {
            try {
                inputFilter = JSON.parse(args[1]);
            } catch (err) {
                console.log(`Filter parsing error!`, err);
                process.exit(1);
            }
            args.splice(0, 2);
            break;
        }
        case "--include-invalid-op": {
            skipInvalidOp = false;
            args.splice(0, 1);
            break;
        }
        case "--print-sensor-data": {
            printSensorData = true;
            args.splice(0, 1);
            break;
        }
        default: {
            outputPath = args[0];
            args.splice(0, 1);
            break;
        }
    }
}

console.log(`Input folder: ${inputFolder}`);
console.log(`Max LTE cell to be displayed: ${lteLimit === -1 ? "no limit" : lteLimit}`);
console.log(`Max Wi-Fi AP to be displayed: ${wifiLimit === -1 ? "no limit" : wifiLimit}`);

let sortWifiRssiDsc = (a,b) => {return b.rssi - a.rssi};
let sortLteRsrpDsc = (a,b) => {return b.rsrp - a.rsrp};
let sumWifiRssi = (a, b) => {return {rssi: a.rssi + b.rssi}};

let maxLteCount = 0;
let maxWifi2_4Count = 0;
let maxWifi5Count = 0;
let locationForHeader;

// Get stats
agg.callbackJsonRecursive(inputFolder, data => {
    if (data.type !== "sigcap") return;
    let sigcap = data.json;
    if (inputFilter) {
        sigcap = filter.filterArray(inputFilter, sigcap);
    }
    console.log(`processing... path: ${data.path}, # of files: ${data.filePath.length}, # of data: ${sigcap.length}`);

    for(let i = 0, length1 = sigcap.length; i < length1; i++){
        if (locationForHeader === undefined) {
            locationForHeader = sigcap[i].location;
        }
        // Get max LTE cells
        if (maxLteCount < sigcap[i].cell_info.length) {
            maxLteCount = sigcap[i].cell_info.length;
        }

        // Get max # of wifi
        let wifi2_4Count = 0;
        let wifi5Count = 0;
        for (let k = 0, length3 = sigcap[i].wifi_info.length; k < length3; k++){
            let currFreq = sigcap[i].wifi_info[k].centerFreq0 ? sigcap[i].wifi_info[k].centerFreq0 : sigcap[i].wifi_info[k].primaryFreq;
            if (currFreq >= 5000) {
                wifi5Count++;
            } else {
                wifi2_4Count++;
            }
        }
        if (maxWifi2_4Count < wifi2_4Count) {
            maxWifi2_4Count = wifi2_4Count;
        }
        if (maxWifi5Count < wifi5Count) {
            maxWifi5Count = wifi5Count;
        }
    }
});


console.log(`Max number of LTE cells: ${maxLteCount}`);
console.log(`Max number of WiFi 2.4 GHz: ${maxWifi2_4Count}`);
console.log(`Max number of WiFi 5 GHz: ${maxWifi5Count}`);

if (lteLimit === -1) {
    lteLimit = maxLteCount;
}
let wifi2_4Limit, wifi5Limit;
if (wifiLimit === -1) {
    wifi2_4Limit = maxWifi2_4Count;
    wifi5Limit = maxWifi5Count;
} else {
    wifi2_4Limit = wifiLimit;
    wifi5Limit = wifiLimit;
}
// Reduce 1 for primary
lteLimit--;

// Write csv
console.log(`Writing to: ${outputPath}`);
let os = fs.createWriteStream(outputPath);

// Write header
let header = `operator,sim_operator,carrier,device_id,device_model,timestamp,latitude,longitude,altitude,hor_acc,ver_acc,network_type,network_icon,nrStatus,nrAvailable,dcNrRestricted,enDcAvailable,nrFrequencyRange,ci,usingCA,cellBandwidths,sumBw,`;
if (printSensorData) {
    header += `deviceTempC,ambientTempC,battCapPerc,battTempC,battVoltageMv,battChargeUah,battCurrAveUa,battCurrNowUa,battEnergyNwh,battStatus,battTechnology,battPresent,`
    header += `hardwareCpuTempC,hardwareGpuTempC,hardwareSkinTempC,hardwareBattTempC,hardwareCpuThrotTempC,hardwareGpuThrotTempC,hardwareSkinThrotTempC,hardwareBattThrotTempC,hardwareCpuShutTempC,hardwareGpuShutTempC,hardwareSkinShutTempC,hardwareBattShutTempC,`;
}
header += `num_of_lte_cell,lte_primary_pci,lte_primary_band,lte_primary_earfcn,lte_primary_bandwidth,lte_primary_rsrp,lte_primary_rsrq,lte_primary_rssi,`;
header += `nr_pci,nr_band,nr_earfcn,nr_bandwidth,nr_rsrp,nr_rsrq,`;
for (let i = 1; i <= lteLimit; i++) {
    header += `lte_other${i}_pci,lte_other${i}_band,lte_other${i}_earfcn,lte_other${i}_rsrp,lte_other${i}_rsrq,lte_other${i}_rssi,`;
}
header += `connected_wifi_ssid,connected_wifi_rssi,connected_wifi_freq_mhz,connected_wifi_primary_ch,connected_wifi_bw_mhz,connected_wifi_standard,connected_wifi_tx_link_speed_mbps,connected_wifi_rx_link_speed_mbps,connected_wifi_max_tx_link_speed_mbps,connected_wifi_max_rx_link_speed_mbps,num_of_wifi_2_4,avg_rssi_of_wifi_2_4,`;
for(let i = 1; i <= wifi2_4Limit; i++){
    header += `${i}_wifi_2_4_freq,${i}_wifi_2_4_width,${i}_wifi_2_4_rssi,`;
}
header += `num_of_wifi_5,avg_rssi_of_wifi_5,`;
for(let i = 1; i <= wifi5Limit; i++){
    header += `${i}_wifi_5_freq,${i}_wifi_5_width,${i}_wifi_5_rssi,`;
}
header += `\n`;
os.write(header);

hashBin = [];

// Get data
agg.callbackJsonRecursive(inputFolder, data => {
    if (data.type !== "sigcap") return;
    let sigcap = data.json;
    if (inputFilter) {
        sigcap = filter.filterArray(inputFilter, sigcap);
    }
    for(let i = 0, length1 = sigcap.length; i < length1; i++){
        // Sanity checks
        let hash = dataUtils.hashObj(sigcap[i]);
        if (hashBin.includes(hash)) continue;
        hashBin.push(hash);
        if (skipInvalidOp
            && sigcap[i].opName === undefined
            && sigcap[i].simName === undefined
            && sigcap[i].carrierName === undefined) {
            continue;
        }
        let op = dataUtils.getCleanOp(sigcap[i]);
        if (skipInvalidOp && op === "Unknown") continue;
        let datetimeIso = dataUtils.getCleanDatetime(sigcap[i]);
        let networkType = dataUtils.getNetworkType(sigcap[i]);
        // Write overview
        let entry = `${op},${sigcap[i].simName ? sigcap[i].simName : "N/A"},${sigcap[i].carrierName ? sigcap[i].carrierName : "N/A"},${sigcap[i].uuid ? sigcap[i].uuid : "N/A"},${sigcap[i].deviceName ? sigcap[i].deviceName : "N/A"},`
                    + `${datetimeIso},${sigcap[i].location.latitude},${sigcap[i].location.longitude},${sigcap[i].location.altitude},${sigcap[i].location.hor_acc},${sigcap[i].location.ver_acc},`
                    + `${networkType},${dataUtils.getNetworkIcon(sigcap[i])},${dataUtils.getServiceState(sigcap[i], "nrStatus")},${dataUtils.getServiceState(sigcap[i], "nrAvailable")},`
                    + `${dataUtils.getServiceState(sigcap[i], "dcNrRestricted")},${dataUtils.getServiceState(sigcap[i], "enDcAvailable")},${dataUtils.getServiceState(sigcap[i], "nrFrequencyRange")},`
                    + `${dataUtils.getServiceState(sigcap[i], "ci")},"${dataUtils.getServiceState(sigcap[i], "usingCA")}","${dataUtils.getServiceState(sigcap[i], "cellBandwidths")}",${dataUtils.sumCellBw(sigcap[i])},`;
        if (printSensorData) {
            entry += `${dataUtils.getCleanValue(sigcap[i], "sensor.deviceTempC")},${dataUtils.getCleanValue(sigcap[i], "sensor.ambientTempC")},`
                    + `${dataUtils.getCleanValue(sigcap[i], "sensor.battCapPerc")},${dataUtils.getCleanValue(sigcap[i], "sensor.battTempC")},`
                    + `${dataUtils.getCleanValue(sigcap[i], "sensor.battVoltageMv")},${dataUtils.getCleanValue(sigcap[i], "sensor.battChargeUah")},`
                    + `${dataUtils.getCleanValue(sigcap[i], "sensor.battCurrAveUa")},${dataUtils.getCleanValue(sigcap[i], "sensor.battCurrNowUa")},`
                    + `${dataUtils.getCleanValue(sigcap[i], "sensor.battEnergyNwh")},${dataUtils.getCleanValue(sigcap[i], "sensor.battStatus")},`
                    + `${dataUtils.getCleanValue(sigcap[i], "sensor.battTechnology")},${dataUtils.getCleanValue(sigcap[i], "sensor.battPresent")},`
                    + `${dataUtils.getCleanValue(sigcap[i], "sensor.hardwareCpuTempC")},${dataUtils.getCleanValue(sigcap[i], "sensor.hardwareGpuTempC")},`
                    + `${dataUtils.getCleanValue(sigcap[i], "sensor.hardwareSkinTempC")},${dataUtils.getCleanValue(sigcap[i], "sensor.hardwareBattTempC")},`
                    + `${dataUtils.getCleanValue(sigcap[i], "sensor.hardwareCpuThrotTempC")},${dataUtils.getCleanValue(sigcap[i], "sensor.hardwareGpuThrotTempC")},`
                    + `${dataUtils.getCleanValue(sigcap[i], "sensor.hardwareSkinThrotTempC")},${dataUtils.getCleanValue(sigcap[i], "sensor.hardwareBattThrotTempC")},`
                    + `${dataUtils.getCleanValue(sigcap[i], "sensor.hardwareCpuShutTempC")},${dataUtils.getCleanValue(sigcap[i], "sensor.hardwareGpuShutTempC")},`
                    + `${dataUtils.getCleanValue(sigcap[i], "sensor.hardwareSkinShutTempC")},${dataUtils.getCleanValue(sigcap[i], "sensor.hardwareBattShutTempC")},`;
        }

        entry += `${sigcap[i].cell_info.length},`;
        // Get LTE primary
        let found = false;
        for(let j = 0, length2 = sigcap[i].cell_info.length; j < length2; j++){
            if (dataUtils.isLtePrimary(sigcap[i].cell_info[j])) {
                found = true;
                // Write primary PCI-Band-EARFCN, bandwidth, rsrp, rsrq, rssi
                entry += `${sigcap[i].cell_info[j].pci},${sigcap[i].cell_info[j].band},${sigcap[i].cell_info[j].earfcn},${sigcap[i].cell_info[j].width},`;
                entry += `${dataUtils.cleanSignal(sigcap[i].cell_info[j].rsrp)},`
                        + `${dataUtils.cleanSignal(sigcap[i].cell_info[j].rsrq)},`
                        + `${dataUtils.cleanSignal(sigcap[i].cell_info[j].rssi)},`;
                // Delete entry j
                sigcap[i].cell_info.splice(j, 1);
                break;
            }
        }
        if (!found) {
            if (sigcap[i].nr_info && sigcap[i].nr_info.length > 0) {
                // Maybe NR-SA
                // if (networkType === "NR-NSA") continue;
                entry += `,,,,,,,`;
            } else if (skipInvalidOp) {
                // Skip file
                continue;
            } else {
                entry += `,,,,,,,`;
            }
        }

        // Get NR, assume only one NR cell
        if (sigcap[i].nr_info && sigcap[i].nr_info.length > 0) {
            entry += `${sigcap[i].nr_info[0].pciNr ? sigcap[i].nr_info[0].pciNr : ""},${sigcap[i].nr_info[0].band ? sigcap[i].nr_info[0].band : ""},`
                    + `${sigcap[i].nr_info[0].nrarfcn ? sigcap[i].nr_info[0].nrarfcn : ""},${sigcap[i].nr_info[0].width ? sigcap[i].nr_info[0].width : ""},`
                    + `${dataUtils.cleanSignal(sigcap[i].nr_info[0].ssRsrp)},${dataUtils.cleanSignal(sigcap[i].nr_info[0].ssRsrq)},`;
        } else {
            entry += `,,,,,,`;
        }

        // Sort descending on RSRP
        sigcap[i].cell_info.sort(sortLteRsrpDsc);
        // Populate the rest of the cells
        for(let j = 0; j < lteLimit; j++){
            if (j < sigcap[i].cell_info.length) {
                // Write other PCI-Band-EARFCN, rsrp, rsrq, rssi
                entry += `${sigcap[i].cell_info[j].pci},${sigcap[i].cell_info[j].band},${sigcap[i].cell_info[j].earfcn},`;
                entry += `${dataUtils.cleanSignal(sigcap[i].cell_info[j].rsrp)},`
                        + `${dataUtils.cleanSignal(sigcap[i].cell_info[j].rsrq)},`
                        + `${dataUtils.cleanSignal(sigcap[i].cell_info[j].rssi)},`;
            } else {
                entry += `,,,,,,`
            }
        }

        // Get 2.4 and 5 Wifi
        let holder2 = [], holder5 = [], connectedWifi;
        for (let k = 0, length3 = sigcap[i].wifi_info.length; k < length3; k++){
            if (sigcap[i].wifi_info[k].rssi === 2147483647) continue; // wrong value
            if (sigcap[i].wifi_info[k].connected == true) {
                connectedWifi = sigcap[i].wifi_info[k];
            }
            let currFreq = sigcap[i].wifi_info[k].centerFreq0 ? sigcap[i].wifi_info[k].centerFreq0 : sigcap[i].wifi_info[k].primaryFreq;
            if (currFreq < 5000) {
                holder2.push({freq: currFreq, width: sigcap[i].wifi_info[k].width, rssi: dataUtils.cleanSignal(sigcap[i].wifi_info[k].rssi)});
            } else {
                holder5.push({freq: currFreq, width: sigcap[i].wifi_info[k].width, rssi: dataUtils.cleanSignal(sigcap[i].wifi_info[k].rssi)});
            }
        }

        // Write connected Wifi
        if (connectedWifi) {
            entry += `${connectedWifi.ssid},${connectedWifi.rssi},${connectedWifi.centerFreq0},${connectedWifi.primaryChNum},`
                    + `${connectedWifi.width},${connectedWifi.standard},${connectedWifi.txLinkSpeed},${connectedWifi.rxLinkSpeed},`
                    + `${connectedWifi.maxSupportedTxLinkSpeed},${connectedWifi.maxSupportedRxLinkSpeed},`;
        } else {
            entry += `,,,,,,,,,,`;
        }

        // Write Wifi 2.4
        holder2.sort(sortWifiRssiDsc);
        tempAvg = (holder2.length === 0) ? "" : (holder2.reduce(sumWifiRssi).rssi / holder2.length);
        entry += `${holder2.length},${tempAvg},`;
        for (let k = 0, length3 = holder2.length; k < wifi2_4Limit; k++) {
            if (k < length3) {
                entry += `${holder2[k].freq},${holder2[k].width},${holder2[k].rssi},`;
            } else {
                entry += `,,,`;
            }
        }
        // Write Wifi 5
        holder5.sort(sortWifiRssiDsc);
        tempAvg = (holder5.length === 0) ? "" : (holder5.reduce(sumWifiRssi).rssi / holder5.length);
        entry += `${holder5.length},${tempAvg},`;
        for (let k = 0, length3 = holder5.length; k < wifi5Limit; k++) {
            if (k < length3) {
                entry += `${holder5[k].freq},${holder5[k].width},${holder5[k].rssi},`;
            } else {
                entry += `,,,`;
            }
        }

        // EOL and flush
        entry += `\n`;
        os.write(entry);
    }
});


os.close();