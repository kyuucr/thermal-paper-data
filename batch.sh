#!/bin/bash

echo "==========================="
echo "Running node scripts: 001"
node 001a-fccdl-vs-temps.js >> /dev/null 2>&1
node 001b-fccdl-vs-temps-linefit.js >> /dev/null 2>&1
cd plots/001-fccdl-vs-temps
./plotPdf.sh
cd ../..
echo "Done!"
echo "==========================="
echo "Running node scripts: 002"
node 002a-time-vs-nsgdl-temps.js >> /dev/null 2>&1
node 002b-time-vs-nsgdl-temps-normalized.js >> /dev/null 2>&1
node 002c-time-vs-fccdl-temps-normalized.js >> /dev/null 2>&1
cd plots/002-time-vs-nsgdl-temps
./plot.sh
cd ../..
echo "Done!"
echo "==========================="
echo "Running node scripts: 003"
node 003a-cdf-temps-nrch.js >> /dev/null 2>&1
cd plots/003-cdf-temps-nrch
./plot.sh
cd ../..
echo "Done!"
echo "==========================="
