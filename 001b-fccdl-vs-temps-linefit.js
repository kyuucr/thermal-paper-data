const agg = require('./lib/aggregator');
const commandParser = require('./lib/command-parser');
const csvUtils = require('./lib/csv-utils');
const dataUtils = require('./lib/data-utils');
const statsUtils = require('./lib/stats-utils');
const fs = require('fs');
const path = require('path');
const { LinearModel } = require('linear-regression-model');


// Fn declarations
const fileWritten = {};
const writeToFile = function(path, data, header, isAppend = false) {
    if (isAppend && fileWritten[path] === true) {
        fs.appendFileSync(path, data);
    } else {
        if (isAppend) {
            fileWritten[path] = true;
        }
        let fullData = (header ? header : "") + data;
        fs.writeFileSync(path, fullData);
    }
}


// Process arguments
commandParser.addArgument("input dir", { defaultValue: "./plots/001-fccdl-vs-temps/data" });
commandParser.addArgument("output dir", { defaultValue: "./plots/001-fccdl-vs-temps/data" });
const options = commandParser.parse();

const sigcapNrRsrp = { p5:[], s21:[] };
const gnuplotList = agg.getFileListRecursive(options["input dir"], /.*-dlMbps-(skin|cpu)Temp-(lte|nr)\.dat$/);
gnuplotList.forEach(gnuplotFile => {
    const outPath = gnuplotFile.replace(/\.dat$/, "-linefit.dat");
    console.log(`Processing... ${gnuplotFile}`);
    console.log(`Output path: ${outPath}`);

    let tempArr = [], dlMbpsArr = [];
    const inputLines = fs.readFileSync(gnuplotFile).toString().split("\n");
    if (inputLines[0].includes("FCC ST")) {
        inputLines.splice(0, 1);
    }
    inputLines.forEach(line => {
        let split = line.split(" ");
        let dlMbps = parseFloat(split[0]);
        let temp = parseFloat(split[1]);
        if (!isNaN(dlMbps) && !isNaN(temp)) {
            dlMbpsArr.push(dlMbps);
            tempArr.push(temp);
        }
    });

    if (dlMbpsArr.length > 1) {
        // max & min values
        const maxDlMbps = statsUtils.maxArray(dlMbpsArr);
        const minDlMbps = statsUtils.minArray(dlMbpsArr);
        // const maxTemp = statsUtils.maxArray(tempArr);
        // const minTemp = statsUtils.minArray(tempArr);

        // Linear model
        const lm = new LinearModel(dlMbpsArr, tempArr);
        // console.log(lm.getLinearEquation());
        const lineFn = lm.getLinearEquation().function;

        // Write gnuplot data & set up for linear regression
        let stringOutput = `"FCC ST Throughput (Mbps)" "${gnuplotFile.includes("skin") ? "Skin" : "CPU"} Temp (°C)"\n`
                         + `${minDlMbps} ${lineFn(minDlMbps)}\n`
                         + `${maxDlMbps} ${lineFn(maxDlMbps)}\n`;

        fs.writeFileSync(outPath, stringOutput);
    } else {
        console.log(`Skipped for not enough data`);
    }
});

console.log(`Done!`);
