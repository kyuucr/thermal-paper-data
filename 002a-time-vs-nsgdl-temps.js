const agg = require('./lib/aggregator');
const commandParser = require('./lib/command-parser');
const csvUtils = require('./lib/csv-utils');
const dataUtils = require('./lib/data-utils');
const statsUtils = require('./lib/stats-utils');
const fs = require('fs');
const path = require('path');


// Fn & var declarations
const fileWritten = {};
const writeToFile = function(path, data, header, isAppend = false) {
    if (isAppend && fileWritten[path] === true) {
        fs.appendFileSync(path, data);
    } else {
        if (isAppend) {
            fileWritten[path] = true;
        }
        let fullData = (header ? header : "") + data;
        fs.writeFileSync(path, fullData);
    }
}

const headerTput = `"Elapsed time (s)" "PHY Throughput (Mbps)"\n`;
const headerNrCh = `"Elapsed time (s)" "# of NR Channel"\n`;
const headerCpuTemp = `"Elapsed time (s)" "CPU Temp (°C)"\n`;
const headerGpuTemp = `"Elapsed time (s)" "GPU Temp (°C)"\n`;
const headerSkinTemp = `"Elapsed time (s)" "Skin Temp (°C)"\n`;
const bucket = {};
const testIdBin = [];


// Process arguments
commandParser.addArgument("input dir", { defaultValue: "./input" });
commandParser.addArgument("output dir", { defaultValue: "./plots/002-time-vs-nsgdl-temps/data" });
commandParser.addArgument("time bucket", { keyword: "--time-bucket", defaultValue: 5000 });
const options = commandParser.parse();

const metadataList = agg.getFileListRecursive(options["input dir"], /metadata-.*.json$/);
metadataList.forEach(metadataFile => {

    console.log(`Processing... ${metadataFile}`);
    const metadata = JSON.parse(fs.readFileSync(metadataFile));
    const inputDir = metadataFile.replace(new RegExp(`${path.sep}metadata-|\\.json$`, "g"), "/");

    // Process timespan
    let nsgData = {};
    let timespans = metadata.timespans;
    for (let i = 0, length1 = timespans.length; i < length1; i++) {
        timespans[i].start = new Date(timespans[i].start).getTime();
        timespans[i].end = new Date(timespans[i].end).getTime();

        // Check for nsg csv data on each timespan and read it
        if (timespans[i].nsg !== undefined) {
            console.log(`Reading NSG file... ${path.join(inputDir, timespans[i].nsg)}`);
            nsgData[i] = agg.getCsv(path.join(inputDir, timespans[i].nsg));
        }
    }
    // console.log(nsgData)
    const nsgKeys = Object.keys(nsgData);
    if (nsgKeys.length === 0) {
        // No nsg data, skip this file
        return;
    }

    // Skip FCC ST only data
    if (metadata.description.match(/FCC ST Only/i) !== null) {
        return;
    }

    const isWinter = path.basename(inputDir).startsWith("01");
    const isChicago = metadataFile.match(/09-2(8|9)/) === null;
    let pathSplit = inputDir.split(path.sep);
    const outputDir = path.join(options["output dir"], pathSplit[1], pathSplit[2]);
    const prefix = pathSplit[1];
    console.log(`is winter: ${isWinter}; is Chicago: ${isChicago}`);
    console.log(`Desc.: ${metadata.description}`);
    console.log(`Input dir: ${inputDir}`);
    console.log(`Output dir: ${outputDir}`);


    const fccBin = [];

    // Init bucket containing the idx of timespan with nsg data
    const output = {};
    nsgKeys.forEach(idx => {
        bucket[idx] = {
            sigcapNrRsrp: [],
            cpuTemp: [],
            gpuTemp: [],
            skinTemp: [],
            fccFilter: []
        };
    });

    agg.callbackJsonRecursive(inputDir, data => {
        let json = data.json;
        let dataType = dataUtils.getJSONType(json[0]);
        console.log(`Folder: ${data.path}; Data type: ${dataType}; # of data: ${json.length}`);

        if (dataType === "sigcap") {
            for (let i = 0, length1 = json.length; i < length1; i++){
                let time = new Date(json[i].datetimeIso).getTime();
                let idx;
                for (let j = 0, length2 = nsgKeys.length; j < length2; j++) {
                    if (time > timespans[nsgKeys[j]].start && time < timespans[nsgKeys[j]].end) {
                        idx = nsgKeys[j];
                        break;
                    }
                }
                if (idx === undefined) continue;

                if (json[i].sensor) {
                    if (json[i].sensor.hardwareCpuTempC && json[i].sensor.hardwareCpuTempC.length > 0) {
                        bucket[idx].cpuTemp.push({ value: statsUtils.meanArray(json[i].sensor.hardwareCpuTempC), time: time });
                    }
                    if (json[i].sensor.hardwareGpuTempC && json[i].sensor.hardwareGpuTempC.length > 0) {
                        bucket[idx].gpuTemp.push({ value: statsUtils.meanArray(json[i].sensor.hardwareGpuTempC), time: time });
                    }
                    if (json[i].sensor.hardwareSkinTempC && json[i].sensor.hardwareSkinTempC.length > 0) {
                        bucket[idx].skinTemp.push({ value: statsUtils.meanArray(json[i].sensor.hardwareSkinTempC), time: time });
                    }
                }
                if (json[i].nr_info && json[i].nr_info[0]) {
                    bucket[idx].sigcapNrRsrp.push({ value: json[i].nr_info[0].ssRsrp, time: time });
                }
            }
        } else if (dataType == "fcc") {
            // Take FCC DL test time for filtering sigcap and nsg data
            for(let i = 0, length1 = json.length; i < length1; i++) {
                let testId = json[i].tests.testId;
                // Skip duplicates testId
                if (testId === undefined || testIdBin.includes(testId)) continue;
                testIdBin.push(testId);

                // Check if download test exist and not 0
                let downloadTest = json[i].tests.download;
                if (downloadTest === undefined
                        || downloadTest.local_datetime === undefined
                        || downloadTest.throughput === 0) {
                    continue;
                }

                // Get valid time idx
                let time = new Date(downloadTest.local_datetime).getTime();
                let idx;
                for (let j = 0, length2 = nsgKeys.length; j < length2; j++) {
                    if (time > timespans[nsgKeys[j]].start && time < timespans[nsgKeys[j]].end) {
                        idx = nsgKeys[j];
                        break;
                    }
                }
                if (idx === undefined) {
                    continue;
                }
                // Put start time as filter for nsg data
                bucket[idx].fccFilter.push(time);
            }
        }
    }); // End of agg.callbackJsonRecursive

    // Create output folder if not exist
    fs.mkdirSync(outputDir, { recursive: true });

    // time bucket in seconds
    let timeBucket = parseFloat(options["time bucket"]) / 1000;

    // Write gnuplot data
    nsgKeys.forEach(idx => {
        let startTime = timespans[idx].start;

        // Map fccFilter values to relative time since startTime, and convert msec -> sec
        let fccFilter;
        if (bucket[idx].fccFilter.length > 0) {
            fccFilter = bucket[idx].fccFilter.map(time => (time - startTime) / 1000);
        }

        // Put DL Tput and NR Channel
        let header = nsgData[idx].headerObj;
        let stringDlMbpsAll = "", stringDlMbpsNr = "", stringDlMbpsLte = "", stringNrCh = "";
        let prevNrCh = -1, prevTime = -1;
        nsgData[idx].csv.forEach(row => {
            let time = parseFloat(row[header["Elapsed Time (s)"]]);
            let tput = row[header["Total Tput (Mbps)"]];
            let nrCh = parseInt(row[header["# of NR channel"]]);

            let pass = true;
            // Skip FCC filtering if it's not exists
            if (fccFilter !== undefined) {
                // Only put DL tput when passing fccFilter
                pass = fccFilter.some(val => {
                    let diff = val - time;
                    return diff >= 0 && diff < timeBucket;
                });
            }
            if (pass) {
                stringDlMbpsAll += `${time} ${tput}\n`;
                if (nrCh > 0) {
                    stringDlMbpsNr += `${time} ${tput}\n`;
                } else {
                    stringDlMbpsLte += `${time} ${tput}\n`;
                }
            }

            // Put NR Channel if there is change in value
            if (prevNrCh !== nrCh) {
                if (prevTime >= 0) {
                    stringNrCh += `${prevTime} ${prevNrCh}\n`;
                }
                stringNrCh += `${time} ${nrCh}\n`;
                prevNrCh = nrCh;
            }
            prevTime = time;
        });
        // put last nrCh value
        if (prevTime >= 0) {
            stringNrCh += `${prevTime} ${prevNrCh}\n`;
        }

        // Put temps
        let stringCpuTemp = "", stringGpuTemp = "", stringSkinTemp = "";
        bucket[idx].cpuTemp.forEach(row => {
            stringCpuTemp += `${(row.time - startTime) / 1e3} ${row.value}\n`;
        });
        bucket[idx].gpuTemp.forEach(row => {
            stringGpuTemp += `${(row.time - startTime) / 1e3} ${row.value}\n`;
        });
        bucket[idx].skinTemp.forEach(row => {
            stringSkinTemp += `${(row.time - startTime) / 1e3} ${row.value}\n`;
        });

        // Write it down
        writeToFile(path.join(outputDir, `${idx}-dlMbps-all.dat`),
                stringDlMbpsAll, headerTput);
        writeToFile(path.join(outputDir, `${idx}-dlMbps-nr.dat`),
                stringDlMbpsNr, headerTput);
        writeToFile(path.join(outputDir, `${idx}-dlMbps-lte.dat`),
                stringDlMbpsLte, headerTput);
        writeToFile(path.join(outputDir, `${idx}-nrCh.dat`),
                stringNrCh, headerNrCh);
        writeToFile(path.join(outputDir, `${idx}-cpuTemp.dat`),
                stringCpuTemp, headerCpuTemp);
        writeToFile(path.join(outputDir, `${idx}-gpuTemp.dat`),
                stringGpuTemp, headerGpuTemp);
        writeToFile(path.join(outputDir, `${idx}-skinTemp.dat`),
                stringSkinTemp, headerSkinTemp);

    }); // End of nsgKeys.forEach
});

console.log(`Done!`);
